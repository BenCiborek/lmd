Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class BusinessDefaults
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.BusinessDefaults",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .BusinessID = c.Int(nullable := False),
                        .Delivery_Zip = c.String(),
                        .Delivery_City = c.String(),
                        .Delivery_State = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.Businesses", Function(t) t.BusinessID, cascadeDelete := True) _
                .Index(Function(t) t.BusinessID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.BusinessDefaults", "BusinessID", "dbo.Businesses")
            DropIndex("dbo.BusinessDefaults", New String() { "BusinessID" })
            DropTable("dbo.BusinessDefaults")
        End Sub
    End Class
End Namespace
