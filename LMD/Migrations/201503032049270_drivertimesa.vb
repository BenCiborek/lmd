Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class drivertimesa
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.DriverTimes", "Email", Function(c) c.String(nullable := False))
            AlterColumn("dbo.DriverTimes", "Phone", Function(c) c.String(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.DriverTimes", "Phone", Function(c) c.String())
            AlterColumn("dbo.DriverTimes", "Email", Function(c) c.String())
        End Sub
    End Class
End Namespace
