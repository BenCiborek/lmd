Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class QBs
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.QBClasses",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .LMRestaurantID = c.String(),
                        .QBClassID = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.QBTokenInfoes",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .AccessToken = c.String(),
                        .accessTokenSecret = c.String(),
                        .consumerKey = c.String(),
                        .consumerSecret = c.String(),
                        .AppToken = c.String(),
                        .CompanyID = c.String()
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            AddColumn("dbo.Jobs", "QBStatus", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "QBStatus")
            DropTable("dbo.QBTokenInfoes")
            DropTable("dbo.QBClasses")
        End Sub
    End Class
End Namespace
