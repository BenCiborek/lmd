Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job2
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Jobs", "DriverID", "dbo.Drivers")
            DropIndex("dbo.Jobs", New String() { "DriverID" })
        End Sub
        
        Public Overrides Sub Down()
            CreateIndex("dbo.Jobs", "DriverID")
            AddForeignKey("dbo.Jobs", "DriverID", "dbo.Drivers", "ID", cascadeDelete := True)
        End Sub
    End Class
End Namespace
