Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Jobs",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .CreateDate = c.DateTime(nullable := False),
                        .JobStatus = c.Int(nullable := False),
                        .BusinessID = c.Int(nullable := False),
                        .EnterUserID = c.Int(nullable := False),
                        .ScheduledPickupDate = c.DateTime(nullable := False),
                        .ActualPickupDate = c.DateTime(nullable := False),
                        .ActualDeliveryDate = c.DateTime(nullable := False),
                        .NotesToDriver = c.String(),
                        .DeliveryNotes = c.String(),
                        .DriverID = c.Int(nullable := False),
                        .DeliveryName = c.String(nullable := False, maxLength := 50),
                        .DeliveryPhone = c.String(nullable := False, maxLength := 15),
                        .DeliveryAddress = c.String(nullable := False, maxLength := 50),
                        .DeliveryAddress2 = c.String(),
                        .DeliveryCity = c.String(nullable := False, maxLength := 50),
                        .DeliveryState = c.String(nullable := False, maxLength := 2),
                        .DeliveryZip = c.String(nullable := False, maxLength := 10),
                        .EnterUser_UserID = c.Int()
                    }) _
                .PrimaryKey(Function(t) t.ID) _
                .ForeignKey("dbo.Businesses", Function(t) t.BusinessID, cascadeDelete := True) _
                .ForeignKey("dbo.Drivers", Function(t) t.DriverID, cascadeDelete := True) _
                .ForeignKey("dbo.Users", Function(t) t.EnterUser_UserID) _
                .Index(Function(t) t.BusinessID) _
                .Index(Function(t) t.DriverID) _
                .Index(Function(t) t.EnterUser_UserID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropForeignKey("dbo.Jobs", "EnterUser_UserID", "dbo.Users")
            DropForeignKey("dbo.Jobs", "DriverID", "dbo.Drivers")
            DropForeignKey("dbo.Jobs", "BusinessID", "dbo.Businesses")
            DropIndex("dbo.Jobs", New String() { "EnterUser_UserID" })
            DropIndex("dbo.Jobs", New String() { "DriverID" })
            DropIndex("dbo.Jobs", New String() { "BusinessID" })
            DropTable("dbo.Jobs")
        End Sub
    End Class
End Namespace
