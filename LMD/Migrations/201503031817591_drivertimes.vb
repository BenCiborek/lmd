Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class drivertimes
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.DriverTimes",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .DriverID = c.Int(nullable := False),
                        .StartTime = c.DateTime(nullable := False),
                        .EndTime = c.DateTime(nullable := False),
                        .Type = c.Int(nullable := False),
                        .Email = c.String(),
                        .Phone = c.String(),
                        .LastAlerted = c.DateTime(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.DriverTimes")
        End Sub
    End Class
End Namespace
