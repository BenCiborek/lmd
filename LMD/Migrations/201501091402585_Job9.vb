Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job9
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "DeliveryType", Function(c) c.String(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "DeliveryType")
        End Sub
    End Class
End Namespace
