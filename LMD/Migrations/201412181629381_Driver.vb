Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Driver
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Drivers",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .Name = c.String(nullable := False, maxLength := 50),
                        .Address1 = c.String(nullable := False, maxLength := 50),
                        .Address2 = c.String(maxLength := 50),
                        .City = c.String(nullable := False, maxLength := 50),
                        .State = c.String(nullable := False, maxLength := 2),
                        .Zip = c.String(nullable := False, maxLength := 10),
                        .Phone = c.String(nullable := False, maxLength := 15),
                        .Inactive = c.Boolean(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            AddColumn("dbo.Businesses", "Phone", Function(c) c.String(nullable := False, maxLength := 15))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Businesses", "Phone")
            DropTable("dbo.Drivers")
        End Sub
    End Class
End Namespace
