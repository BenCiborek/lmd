Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job7
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "DeliveryNumber", Function(c) c.String())
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "DeliveryNumber")
        End Sub
    End Class
End Namespace
