Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            DropForeignKey("dbo.Jobs", "EnterUser_UserID", "dbo.Users")
            DropIndex("dbo.Jobs", New String() { "EnterUser_UserID" })
            AddColumn("dbo.Jobs", "EnterUserName", Function(c) c.String())
            DropColumn("dbo.Jobs", "EnterUserID")
            DropColumn("dbo.Jobs", "EnterUser_UserID")
        End Sub
        
        Public Overrides Sub Down()
            AddColumn("dbo.Jobs", "EnterUser_UserID", Function(c) c.Int())
            AddColumn("dbo.Jobs", "EnterUserID", Function(c) c.Int(nullable := False))
            DropColumn("dbo.Jobs", "EnterUserName")
            CreateIndex("dbo.Jobs", "EnterUser_UserID")
            AddForeignKey("dbo.Jobs", "EnterUser_UserID", "dbo.Users", "UserID")
        End Sub
    End Class
End Namespace
