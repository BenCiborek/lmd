Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class GeoCode
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "DeliveryAddressLatitude", Function(c) c.Double(nullable := False))
            AddColumn("dbo.Jobs", "DeliveryAddressLongitude", Function(c) c.Double(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "DeliveryAddressLongitude")
            DropColumn("dbo.Jobs", "DeliveryAddressLatitude")
        End Sub
    End Class
End Namespace
