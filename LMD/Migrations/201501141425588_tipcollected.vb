Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class tipcollected
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "TipCollected", Function(c) c.Double(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "TipCollected")
        End Sub
    End Class
End Namespace
