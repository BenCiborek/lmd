Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class drivertimesa1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AlterColumn("dbo.DriverTimes", "Phone", Function(c) c.String(nullable := False, maxLength := 15))
        End Sub
        
        Public Overrides Sub Down()
            AlterColumn("dbo.DriverTimes", "Phone", Function(c) c.String(nullable := False))
        End Sub
    End Class
End Namespace
