Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job8
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "MoneyToCollect", Function(c) c.Double(nullable := False))
            AddColumn("dbo.Jobs", "MoneyCollected", Function(c) c.Double(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "MoneyCollected")
            DropColumn("dbo.Jobs", "MoneyToCollect")
        End Sub
    End Class
End Namespace
