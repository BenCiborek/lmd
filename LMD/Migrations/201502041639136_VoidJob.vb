Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class VoidJob
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "VoidDate", Function(c) c.DateTime(nullable := False))
            AddColumn("dbo.Jobs", "VoidReason", Function(c) c.String())
            AddColumn("dbo.Jobs", "VoidUser", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "VoidUser")
            DropColumn("dbo.Jobs", "VoidReason")
            DropColumn("dbo.Jobs", "VoidDate")
        End Sub
    End Class
End Namespace
