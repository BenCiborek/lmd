Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job4
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Users", "DriverID", Function(c) c.Int(nullable := False))
            AddColumn("dbo.Users", "BusinessID", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Users", "BusinessID")
            DropColumn("dbo.Users", "DriverID")
        End Sub
    End Class
End Namespace
