Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Business
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Businesses",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .LMID = c.Int(nullable := False),
                        .Name = c.String(nullable := False, maxLength := 50),
                        .Address1 = c.String(nullable := False, maxLength := 50),
                        .Address2 = c.String(maxLength := 50),
                        .City = c.String(nullable := False, maxLength := 50),
                        .State = c.String(nullable := False, maxLength := 2),
                        .Zip = c.String(nullable := False, maxLength := 10)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Businesses")
        End Sub
    End Class
End Namespace
