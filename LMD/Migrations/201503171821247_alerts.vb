Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class alerts
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.AlertMessages",
                Function(c) New With
                    {
                        .Id = c.Int(nullable := False, identity := True),
                        .Type = c.String(nullable := False),
                        .Message = c.String(nullable := False),
                        .CssClass = c.String(nullable := False),
                        .Startdate = c.DateTime(nullable := False),
                        .EndDate = c.DateTime(nullable := False),
                        .Active = c.Boolean(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.Id)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.AlertMessages")
        End Sub
    End Class
End Namespace
