Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Business1
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Businesses", "Inactive", Function(c) c.Boolean(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Businesses", "Inactive")
        End Sub
    End Class
End Namespace
