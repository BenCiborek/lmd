Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Start
        Inherits DbMigration
    
        Public Overrides Sub Up()
            CreateTable(
                "dbo.Emails",
                Function(c) New With
                    {
                        .ID = c.Int(nullable := False, identity := True),
                        .InteractionType = c.String(maxLength := 1),
                        .InteractionNumber = c.Int(nullable := False),
                        .Email_To = c.String(),
                        .Email_From = c.String(),
                        .Email_Subject = c.String(),
                        .Email_Body = c.String(),
                        .Email_Attachment_bln = c.Boolean(nullable := False),
                        .Email_Attachment_Path = c.String(),
                        .Email_Create_Date = c.DateTime(nullable := False),
                        .Email_Status = c.Int(nullable := False),
                        .Email_Sent_Date = c.DateTime(nullable := False),
                        .Email_Type = c.Int(nullable := False)
                    }) _
                .PrimaryKey(Function(t) t.ID)
            
            CreateTable(
                "dbo.Users",
                Function(c) New With
                    {
                        .UserID = c.Int(nullable := False, identity := True),
                        .UserName = c.String(),
                        .Email = c.String(nullable := False),
                        .password = c.String(),
                        .Roles = c.String()
                    }) _
                .PrimaryKey(Function(t) t.UserID)
            
        End Sub
        
        Public Overrides Sub Down()
            DropTable("dbo.Users")
            DropTable("dbo.Emails")
        End Sub
    End Class
End Namespace
