Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class Job6
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "DeliveryDate", Function(c) c.DateTime(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "DeliveryDate")
        End Sub
    End Class
End Namespace
