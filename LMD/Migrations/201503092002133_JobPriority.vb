Imports System
Imports System.Data.Entity.Migrations
Imports Microsoft.VisualBasic

Namespace Migrations
    Public Partial Class JobPriority
        Inherits DbMigration
    
        Public Overrides Sub Up()
            AddColumn("dbo.Jobs", "Priority", Function(c) c.Int(nullable := False))
        End Sub
        
        Public Overrides Sub Down()
            DropColumn("dbo.Jobs", "Priority")
        End Sub
    End Class
End Namespace
