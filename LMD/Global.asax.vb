﻿' Note: For instructions on enabling IIS6 or IIS7 classic mode, 
' visit http://go.microsoft.com/?LinkId=9394802
Imports System.Web.Http
Imports System.Web.Optimization
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure


Public Class MvcApplication
    Inherits System.Web.HttpApplication
    Sub Application_Start()
        AreaRegistration.RegisterAllAreas()



        WebApiConfig.Register(GlobalConfiguration.Configuration)
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        BundleConfig.RegisterBundles(BundleTable.Bundles)
        AuthConfig.RegisterAuth()
    End Sub
    Protected Sub FormsAuthentication_OnAuthenticate(sender As [Object], e As FormsAuthenticationEventArgs)
        If FormsAuthentication.CookiesSupported = True Then
            If Request.Cookies(FormsAuthentication.FormsCookieName) IsNot Nothing Then
                Try
                    'let us take out the username now                
                    Dim username As String = FormsAuthentication.Decrypt(Request.Cookies(FormsAuthentication.FormsCookieName).Value).Name
                    Dim roles As String = String.Empty

                    Using entities As New LMDEntities
                        Dim user As User = entities.Users.SingleOrDefault(Function(u) u.UserName = username)

                        roles = user.Roles
                    End Using
                    'let us extract the roles from our own custom cookie


                    'Let us set the Pricipal with our user specific details
                    e.User = New System.Security.Principal.GenericPrincipal(New System.Security.Principal.GenericIdentity(username, "Forms"), roles.Split(";"c))
                    'somehting went wrong
                Catch generatedExceptionName As Exception
                End Try
            End If
        End If
    End Sub
End Class
