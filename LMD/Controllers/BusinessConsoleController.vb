﻿<Authorize()> _
    Public Class BusinessConsoleController
    Inherits System.Web.Mvc.Controller

    Dim db As LMDEntities = New LMDEntities

    '
    ' GET: /BusinessConsole

    Function Index() As ActionResult
        Dim u As New UserModel
        Dim Loggedinuser = u.getUser(User.Identity.Name)

        Dim Business = db.Businesses.Find(Loggedinuser.BusinessID)

        Return View(Business)
    End Function
    Function CloseoutReport(ByVal start As DateTime, ByVal finish As DateTime)
        Dim u As New UserModel
        Dim Loggedinuser = u.getUser(User.Identity.Name)

        Dim CRVM As New BusinessCloseOutReportVM

        CRVM.StartDate = start
        CRVM.EndDate = finish

        CRVM.Jobs = db.Jobs.Where(Function(x) x.BusinessID = Loggedinuser.BusinessID And
                                      x.JobStatus = 4 And
                                      x.ScheduledPickupDate >= CRVM.StartDate And
                                      x.ScheduledPickupDate < CRVM.EndDate).ToList
        Return View(CRVM)
    End Function
    Function GetCloseoutReportDropDown(ByVal CODD As CloseoutDateDropDownVM) As ActionResult
        Return View(CODD)
    End Function
End Class