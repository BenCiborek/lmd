﻿Imports System.Data.Entity
Imports LMD

<Authorize()> _
Public Class AdminController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Admin
    Private db As LMDEntities = New LMDEntities

    Function Index() As ActionResult
        ' Dim Test = User.IsInRole("User")
        Return View()
    End Function

    Function AdminStats(ByVal dt As DateTime)
        Dim startDate As DateTime = dt
        Dim endDate As DateTime = DateAdd(DateInterval.Day, 1, dt)
        Dim jobs = db.Jobs.Where(Function(x) x.JobStatus = 4 And
                                     x.ScheduledPickupDate > startDate And x.ScheduledPickupDate < endDate).ToList
        Dim stats As New AdminStatsVM
        If jobs.Count > 0 Then
            stats.AvgPickupTime = jobs.Average(Function(x) DateDiff(DateInterval.Minute, x.ScheduledPickupDate, TimeZoneInfo.ConvertTimeFromUtc(x.ActualPickupDate, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))))
            stats.MaxPickupTime = jobs.Max(Function(x) DateDiff(DateInterval.Minute, x.ScheduledPickupDate, TimeZoneInfo.ConvertTimeFromUtc(x.ActualPickupDate, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))))
            stats.AvgTimeInCar = jobs.Average(Function(x) DateDiff(DateInterval.Minute, x.ActualPickupDate, x.DeliveryDate))
            stats.MaxTimeInCar = jobs.Max(Function(x) DateDiff(DateInterval.Minute, x.ActualPickupDate, x.DeliveryDate))
            stats.AvgTimeFromEntryToDelivery = jobs.Average(Function(x) DateDiff(DateInterval.Minute, x.CreateDate, x.DeliveryDate))
            stats.MaxTimeFromEntryToDelivery = jobs.Max(Function(x) DateDiff(DateInterval.Minute, x.CreateDate, x.DeliveryDate))
        End If

        Return View(stats)
    End Function

    Function AdminMoney() As ActionResult
        Dim jobs = db.Jobs.ToList()
        Return View(jobs)
    End Function

    Function CashReport(ByVal rundate As String, ByVal StartDate As String) As ActionResult
        Dim d As New DriverModel
        Dim DriverCash As List(Of DriverCashVM) = d.DriverCashoutReport(rundate, startdate:=StartDate)

        Dim DriverCashReport As New List(Of DriverCashVM)

        Dim Drivers = DriverCash.Select(Function(x) x.DriverID).Distinct()

        For Each Driver In Drivers
            Dim DriverCashReportLine As New DriverCashVM
            DriverCashReportLine.DeliveryDate = rundate
            DriverCashReportLine.DriverID = Driver
            DriverCashReportLine.Jobs = DriverCash.Where(Function(x) x.DriverID = Driver).Sum(Function(x) x.Jobs)
            DriverCashReportLine.MoneyCollected = DriverCash.Where(Function(x) x.DriverID = Driver).Sum(Function(x) x.MoneyCollected)
            DriverCashReportLine.MoneyToCollect = DriverCash.Where(Function(x) x.DriverID = Driver).Sum(Function(x) x.MoneyToCollect)
            DriverCashReportLine.TipCollected = DriverCash.Where(Function(x) x.DriverID = Driver).Sum(Function(x) x.TipCollected)
            DriverCashReport.Add(DriverCashReportLine)
        Next




        ViewBag.rundate = rundate
        ViewBag.StartDate = StartDate

        Return View(DriverCashReport)
    End Function
    Function BusinessReport(ByVal rundate As String, ByVal StartDate As String) As ActionResult
        'Get All Jobs for time Period
        Dim jobs = db.Jobs.Where(Function(x) x.JobStatus = 4 And x.ScheduledPickupDate > StartDate And x.ScheduledPickupDate < rundate).ToList

        'Get all businesses in job
        Dim businesses = jobs.Select(Function(x) x.Business).Distinct()

        Dim Report As New List(Of AdminBussinessSummaryReportVM)

        For Each Business In businesses
            Dim ReportRow As New AdminBussinessSummaryReportVM
            Dim businessJobs = jobs.Where(Function(x) x.BusinessID = Business.ID)

            ReportRow.Business = Business
            ReportRow.Jobs = businessJobs.Count()
            ReportRow.FeesDue = businessJobs.Sum(Function(x) x.RestaurntFee)
            ReportRow.CashCollected = businessJobs.Where(Function(x) x.DeliveryType = "Collect Cash").Sum(Function(x) x.MoneyToCollect)

            Report.Add(ReportRow)
        Next

        ViewBag.rundate = rundate
        ViewBag.StartDate = StartDate

        Return View(Report)
    End Function
    Function AlertMessage()
        Return View()
    End Function

    Function showAlertMessages()
        Dim messages = db.AlertMessages.ToList()

        Return View(messages)
    End Function

    Function AddAlertMessage()

        Return View()
    End Function
    <HttpPost()> _
    Function AddAlertMessage(ByVal Model As AlertMessage)
        If ModelState.IsValid Then
            db.AlertMessages.Add(Model)
            db.SaveChanges()
            Return RedirectToAction("AlertMessage")
        Else
            Return View()
        End If
    End Function

    Function EditAlertMessage(ByVal id As Integer)
        Dim alert = db.AlertMessages.Find(id)
        Return View(alert)
    End Function

    <HttpPost()> _
    Function EditAlertMessage(ByVal model As AlertMessage)
        If ModelState.IsValid Then
            db.Entry(model).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("AlertMessage")
        Else
            Return View()
        End If
    End Function

    Function AdminNav()
        Return View()
    End Function

    Function CurrentSetting()
        Dim CS As New CurrentSettingsVM
        CS.Offset = JobModel.getStartTimeOffset(0)
        CS.NumberofDrivers = DriverModel.driversCount
        Return View(CS)
    End Function


End Class