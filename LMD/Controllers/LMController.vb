﻿Public Class LMController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /LM
    Private db As LMDEntities = New LMDEntities
    Function Index() As ActionResult
        Return View()
    End Function

    Function AddJobVer1(ByVal LMID As Integer,
                        ByVal ScheduledPickupTime As String,
                        ByVal OrderNumber As String,
                        ByVal OrderTotal As Double,
                        ByVal Tip As Double,
                        ByVal Notes As String,
                        ByVal DeliveryName As String,
                        ByVal DeliveryAddress1 As String,
                        ByVal DeliveryAddress2 As String,
                        ByVal DeliveryCity As String,
                        ByVal DeliveryState As String,
                        ByVal DeliveryZip As String,
                        ByVal DeliveryPhone As String)

        Dim b As New BusinessModel
        Dim business = b.getBussinessByLMID(LMID)

        Dim job As New Job
        job.DeliveryNumber = OrderNumber

        job.BusinessID = business.ID

        job.DriverID = DriverModel.getDriverForBusiness(business.ID)
        If job.DriverID <> 0 Then
            'If on a driver Assign it
            job.JobStatus = 2
        Else
            'No Driver Assigned Leave it open
            job.JobStatus = 1
        End If

        Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
        Dim Offset = JobModel.getStartTimeOffset(business.ID)

        'job.ScheduledPickupDate = CDate(ScheduledPickupTime)
        job.ScheduledPickupDate = DateAdd(DateInterval.Minute, Offset, EasternNow)
        job.MoneyToCollect = OrderTotal
        job.TipCollected = Tip
        job.NotesToDriver = Notes
        job.DeliveryName = Left(DeliveryName, 50)
        job.DeliveryAddress = Left(DeliveryAddress1, 50)
        job.DeliveryAddress2 = Left(DeliveryAddress2, 50)
        job.DeliveryCity = Left(DeliveryCity, 50)
        job.DeliveryState = Left(DeliveryState, 2)
        job.DeliveryZip = Left(DeliveryZip, 10)
        job.DeliveryPhone = Left(DeliveryPhone, 15)

        'Lat/Lon
        Dim Geocode As GeoCode = JobModel.AddressToGeocode(job.DeliveryAddress & ", " & job.DeliveryCity & ", " & job.DeliveryState & ", " & job.DeliveryZip)

        job.DeliveryAddressLatitude = Geocode.Lat
        job.DeliveryAddressLongitude = Geocode.Lon

        'Default Dates
        job.ActualDeliveryDate = #1/1/1900#
        job.ActualPickupDate = #1/1/1900#
        job.DeliveryDate = #1/1/1900#
        job.VoidDate = #1/1/1900#
        job.ExpectedArrivalTime = #1/1/1900#
        job.CreateDate = Now.ToUniversalTime
        job.RestaurntFee = 0
        job.DeliveryType = "Local Munchies - Link"
        Dim strxml As String = ""
        strxml &= "<job>"
        Try
            db.Jobs.Add(job)
            db.SaveChanges()
            strxml &= "<status>1</status>"
            strxml &= "<ID>" & job.ID & "</ID>"
            strxml &= "<Message>Successful</Message>"
        Catch ex As Exception
            strxml &= "<status>0</status>"
            strxml &= "<ID>0</ID>"
            strxml &= "<Message>" & HttpContext.Request.Url.AbsoluteUri & " - " & ex.ToString & "</Message>"
        End Try
        strxml &= "</job>"

        Return Me.Content(strxml, "text/xml")
    End Function

End Class