﻿<Authorize()> _
Public Class HomeController
    Inherits System.Web.Mvc.Controller
    Dim db As LMDEntities = New LMDEntities
    Function Index() As ActionResult
        Dim Loggedinuser = UserModel.getUser(User.Identity.Name)

        Select Case Loggedinuser.Roles
            Case "Driver"
                Return RedirectToAction("Index", "DriverConsole")
            Case "Business"
                Return RedirectToAction("Index", "BusinessConsole")
            Case "Admin"
                Return RedirectToAction("Index", "Admin")

        End Select

        Return View(Loggedinuser)
    End Function
    Function ShowAlert(ByVal type As String)
        Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
        Dim alert = db.AlertMessages.Where(Function(x) x.Active = True And
                                               x.Type = type And
                                               x.Startdate < EasternNow And
                                               x.EndDate > EasternNow).ToList
        Return View(alert)
    End Function

End Class
