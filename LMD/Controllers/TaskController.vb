﻿Imports System.Data.Entity
Imports Intuit
Public Class TaskController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Task
    Dim db As LMDEntities = New LMDEntities
    Dim ManagerEmail As String = "sjones@localmunchies.com,njones@localmunchies.com,bennyc66@gmail.com"
    Function Index() As ActionResult
        Return View()
    End Function

    Function NewJobAlert() As ActionResult
        Dim Jobs = DriverModel.getAvailableOpenJobs()
        If Jobs.Count > 0 Then
            'If there is a driver to alert
            Dim Deliveries = db.Jobs.Where(Function(x) {2, 3}.Contains(x.JobStatus)).ToList
            Dim DeliveringDrivers = Deliveries.Select(Function(x) x.DriverID)
            Dim timenow As DateTime = Now().ToUniversalTime
            Dim DriversOn = db.DriverTimes.Where(Function(x) x.StartTime < timenow And
                                                  x.EndTime > timenow).ToList()
            Dim Alerts = DriversOn.Where(Function(x) Not DeliveringDrivers.Contains(x.DriverID)).ToList()

            If Alerts.Count > 0 Then
                'See if we need to send alerts
                For Each alert In Alerts


                    'If they have been alerted in the last 5 min skip it, if not alert them
                    If alert.LastAlerted < DateAdd(DateInterval.Minute, -5, Now().ToUniversalTime) Then
                        'Send Alert
                        Select Case alert.Type
                            Case 1
                                'Email
                                CreateNewJobEmail(alert.Email)
                            Case 2
                                'Text
                                CreateNewJobText(alert.Phone)
                        End Select
                        alert.LastAlerted = Now.ToUniversalTime()
                        db.Entry(alert).State = EntityState.Modified
                        db.SaveChanges()
                    End If
                Next
            End If
            If DriversOn.Count <= 0 Then
                'New Job with no Driver Checked in
                'Alert Managers
                CreateNewJobEmail(ManagerEmail)
            End If
        End If
        Return View()
    End Function
    Function CreateNewJobEmail(ByVal toAddress As String)
        Dim Email As New EmailModel
        Dim emailto = toAddress
        Dim emailfrom = "NoReply@LocalMunchiesDelivery.com"
        Dim emailsubject = "New Job"
        Dim Email_Body = Email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/EmailBody/JobAlertEmail.vbhtml", Nothing, "_Email.vbhtml")

        Email.AddEmail("M", 2, emailto, emailfrom, emailsubject, Email_Body, False, "", 1)
        Return True
    End Function
    Function CreateNewJobText(ByVal toNumber As String)
        SendText(toNumber, ConfigurationManager.AppSettings("PhoneNumber"), "New Delivery: localmunchiesdelivery.com")
        Return True
    End Function

    Function SendText(ByVal toNumber As String, ByVal fromNumber As String, ByVal message As String)
        Dim AccountSid As String = ConfigurationManager.AppSettings("TwilioSID")
        Dim AuthToken As String = ConfigurationManager.AppSettings("TwilioToken")
        Dim twil = New Twilio.TwilioRestClient(AccountSid, AuthToken)



        Dim maketext = twil.SendSmsMessage(fromNumber, toNumber, message)
        If Not maketext.RestException Is Nothing Then

        End If

        Return True
    End Function

    Function SendtoQuickBooks()
        Dim classes = db.QBClasses.ToList()
        Dim tokeninfo = db.QBTokenInfos.Find(1)
        'SendDriverSalesReciept("3/19/2015", "3/20/2015", classes, tokeninfo)
        SendDriverSalesReciept(DateAdd(DateInterval.Day, -1, Now.ToUniversalTime()), Now.ToUniversalTime(), classes, tokeninfo)
    End Function

    'Function SendtoQuickbooksInitialLoad()
    '    Dim BeginingDate As DateTime = #2/20/2015 9:00:00 AM#
    '    Dim classes = db.QBClasses.ToList()
    '    Dim tokeninfo = db.QBTokenInfos.Find(1)
    '    While BeginingDate < #3/23/2015#
    '        SendDriverSalesReciept(BeginingDate, DateAdd(DateInterval.Day, 1, BeginingDate), classes, tokeninfo)
    '        BeginingDate = DateAdd(DateInterval.Day, 1, BeginingDate)
    '    End While
    'End Function
    Function SendDriverSalesReciept(ByVal startDate As DateTime, ByVal endDate As DateTime, ByVal classes As List(Of QBClass), ByVal tokenInfo As QBTokenInfo)
        'This function creates a Sales Reciept 
        'With lines for each business classed by business



        Dim Jobs = db.Jobs.Where(Function(x) x.JobStatus = 4 And
                                     x.QBStatus = 0 And
                                     x.DeliveryDate > startDate And x.DeliveryDate < endDate
                                     ).ToList()



        Dim Drivers = Jobs.Select(Function(x) x.DriverID).Distinct()

        For Each Driver In Drivers
            Dim DriverM = db.Drivers.Find(Driver)
            Dim NewDriverSR As New QBSalesReceipt
            NewDriverSR.DocumentNumber = startDate.ToString("yyyyMMdd") & "-" & Driver
            NewDriverSR.DocumentDate = startDate
            NewDriverSR.PaymentMethodId = 1

            NewDriverSR.BAName = DriverM.Name
            NewDriverSR.BAAddr1 = DriverM.Address1
            NewDriverSR.BAAddr2 = DriverM.Address2
            NewDriverSR.BACity = DriverM.City
            NewDriverSR.BAState = DriverM.State
            NewDriverSR.BAZip = DriverM.Zip



            Dim lines As New List(Of QBLine)

            Dim Businesses = Jobs.Where(Function(y) y.DriverID = Driver).Select(Function(x) x.BusinessID).Distinct()
            For Each business In Businesses

                Dim bus = db.Businesses.Find(business)

                Dim SRLine As New QBLine
                SRLine.ItemID = 4
                SRLine.Amount = 0

                'Get Class Info
                Dim QBClass = db.QBClasses.Where(Function(x) x.LMRestaurantID = bus.LMID).FirstOrDefault()

                If QBClass Is Nothing Then
                    QBClass = New QBClass With {.QBClassID = 0}
                End If

                SRLine.ClassID = QBClass.QBClassID

                Dim Transactions = Jobs.Where(Function(x) x.DriverID = Driver And x.BusinessID = business)
                For Each tran In Transactions
                    If {"Collect Cash"}.Contains(tran.DeliveryType) Then
                        SRLine.Amount += tran.MoneyToCollect
                    End If
                Next
                'Add Line
                If SRLine.Amount <> 0 Then
                    lines.Add(SRLine)
                End If
            Next
            NewDriverSR.Lines = lines
            'Add Transaction
            If NewDriverSR.Lines.Count > 0 Then
                sendSRtoQB(NewDriverSR, tokenInfo)
            End If
        Next
        'Mark All as SR Created
        For Each Job In Jobs
            Job.QBStatus = 1
            db.Entry(Job).State = EntityState.Modified
            db.SaveChanges()
        Next

        Return True
    End Function
    Function sendSRtoQB(ByVal QBSR As QBSalesReceipt, ByVal TokenInfo As QBTokenInfo)
        Dim SalesReciept As New Intuit.Ipp.Data.SalesReceipt

        'Dim billtoAddress As Address = Db.Addresses.Find(ticket.BilltoAddress)

        'Static
        SalesReciept.DocNumber = QBSR.DocumentNumber
        SalesReciept.TxnDateSpecified = True
        SalesReciept.TxnDate = QBSR.DocumentDate
        SalesReciept.domain = "QBO"
        SalesReciept.SyncToken = "0"

        Dim paymentmethodRef As New Intuit.Ipp.Data.ReferenceType
        paymentmethodRef.Value = QBSR.PaymentMethodId
        SalesReciept.PaymentMethodRef = paymentmethodRef

        'Address Info
        Dim billaddr As New Intuit.Ipp.Data.PhysicalAddress

        billaddr.Line1 = QBSR.BAName
        billaddr.Line2 = QBSR.BAAddr1
        billaddr.Line3 = QBSR.BAAddr2
        billaddr.City = QBSR.BACity
        billaddr.CountrySubDivisionCode = QBSR.BAState
        billaddr.PostalCode = QBSR.BAZip
        SalesReciept.BillAddr = billaddr

        ''Email Info
        'Dim email As New Intuit.Ipp.Data.EmailAddress
        'email.Address = ticket.BilltoAdd.Email
        'SalesReciept.BillEmail = email


        SalesReciept.Balance = 0
        SalesReciept.ApplyTaxAfterDiscount = False

        Dim QBlines As New List(Of Intuit.Ipp.Data.Line)

        For Each line In QBSR.Lines
            Dim QBLine As New Intuit.Ipp.Data.Line
            Dim QBLineDetail As New Intuit.Ipp.Data.SalesItemLineDetail
            Dim QBLinemRef As New Intuit.Ipp.Data.ReferenceType
            Dim QBlineClassRef As New Intuit.Ipp.Data.ReferenceType

            QBLine.AmountSpecified = True
            QBLine.Amount = line.Amount
            QBLine.DetailTypeSpecified = True
            QBLine.DetailType = Ipp.Data.LineDetailTypeEnum.SalesItemLineDetail

            QBLinemRef.Value = line.ItemID
            QBLineDetail.ItemRef = QBLinemRef
            QBLineDetail.Qty = 1
            QBLineDetail.QtySpecified = True

            If line.ClassID <> 0 Then
                QBlineClassRef.Value = line.ClassID
                QBLineDetail.ClassRef = QBlineClassRef
            End If


            'Assign Sales Item Line Detail to Line Item
            QBLine.AnyIntuitObject = QBLineDetail

            QBlines.Add(QBLine)
        Next


        SalesReciept.Line = QBlines.ToArray()

        Try
            '#If Not Debug Then

            Dim accessToken = TokenInfo.AccessToken
            Dim accessTokenSecret = TokenInfo.accessTokenSecret
            Dim consumerKey = TokenInfo.consumerKey
            Dim consumerSecret = TokenInfo.consumerSecret


            Dim consumerContext = New DevDefined.OAuth.Consumer.OAuthConsumerContext With {
                .ConsumerKey = consumerKey,
                .ConsumerSecret = consumerSecret,
                .SignatureMethod = DevDefined.OAuth.Framework.SignatureMethod.HmacSha1
              }



            Dim oauthValidator As Intuit.Ipp.Security.OAuthRequestValidator = New Intuit.Ipp.Security.OAuthRequestValidator(accessToken, accessTokenSecret, consumerKey, consumerSecret)

            Dim AppToken = TokenInfo.AppToken
            Dim CompanyID = TokenInfo.CompanyID

            Dim Context As Intuit.Ipp.Core.ServiceContext = New Intuit.Ipp.Core.ServiceContext(AppToken, CompanyID, Intuit.Ipp.Core.IntuitServicesType.QBO, oauthValidator)


            ' Dim added = Intuit.Ipp.Data
            Dim Service As New Ipp.DataService.DataService(Context)
            Dim x = Service.Add(SalesReciept)
            '#End If
        Catch ex As Intuit.Ipp.Exception.IdsException
            Dim sendemail As New EmailModel
            sendemail.AddEmail("E", "1", ManagerEmail, "NoReply@Localmunchies.com", "Error Adding LMD to QucikBooks: " & QBSR.DocumentNumber, ex.ToString, False, "", 1)
            ' Next
        End Try
    End Function


    Function SendBusinessInvoice(ByVal startDate As DateTime, ByVal endDate As DateTime)

        Dim Jobs = db.Jobs.Where(Function(x) x.JobStatus = 4 And
                             x.QBStatus = 1 And
                             x.DeliveryDate > startDate And x.DeliveryDate < endDate).ToList()


        'Mark All As Invoice Created
    End Function

    Function QBReadClassesToTable()
        Try


            Dim TokenInfo = Db.QBTokenInfos.Find(1)

            Dim accessToken = TokenInfo.AccessToken
            Dim accessTokenSecret = TokenInfo.accessTokenSecret
            Dim consumerKey = TokenInfo.consumerKey
            Dim consumerSecret = TokenInfo.consumerSecret


            Dim consumerContext = New DevDefined.OAuth.Consumer.OAuthConsumerContext With {
                .ConsumerKey = consumerKey,
                .ConsumerSecret = consumerSecret,
                .SignatureMethod = DevDefined.OAuth.Framework.SignatureMethod.HmacSha1
              }



            Dim oauthValidator As Intuit.Ipp.Security.OAuthRequestValidator = New Intuit.Ipp.Security.OAuthRequestValidator(accessToken, accessTokenSecret, consumerKey, consumerSecret)

            Dim AppToken = TokenInfo.AppToken
            Dim CompanyID = TokenInfo.CompanyID

            Dim Context As Intuit.Ipp.Core.ServiceContext = New Intuit.Ipp.Core.ServiceContext(AppToken, CompanyID, Intuit.Ipp.Core.IntuitServicesType.QBO, oauthValidator)

            Dim Service As New Ipp.DataService.DataService(Context)

            Dim Classes = Service.FindAll(New Intuit.Ipp.Data.Class())

            ' Dim connstr = System.Configuration.ConfigurationManager.ConnectionStrings("FBEntities").ConnectionString
            'Delete from the table
            '  Db_Execute("Delete From QBClasses", connstr)
            'Reset ID
            ' Db_Execute("DBCC CHECKIDENT ('QBClasses', RESEED, 0)", connstr)

            For Each Cls In Classes

                Dim QBclass As New QBClass With {
                    .QBClassID = Cls.Id,
                    .LMRestaurantID = Trim(Left(Cls.Name, InStr(Cls.Name, "-") - 1))}

                Db.QBClasses.Add(QBclass)
                Db.SaveChanges()
            Next
        Catch ex As Exception
            Dim email As New EmailModel
            email.AddEmail("E", "1", ManagerEmail, "NoReply@Localmunchies.com", "Error Adding ID's From QuickBooks LMD", ex.ToString, False, "", 1)

        End Try
        Return True
    End Function

End Class