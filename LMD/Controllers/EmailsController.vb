﻿Imports System.Data.Entity
Imports System.Net
Imports System.Net.NetworkInformation

'Imports System.Net.Mail
'Imports System.Net
Public Class EmailsController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Emails
    Dim db As LMDEntities = New LMDEntities


    Dim ManagerEmail As String = "sjones@localmunchies.com,njones@localmunchies.com,bennyc66@gmail.com"
    Dim ITEmail As String = "bennyc66@gmail.com"


    Function Send() As ActionResult
        Dim EmailSendVM As New EmailSendVM

        Dim emails = db.Emails.Where(Function(x) x.Email_Status = 1).ToList

        For Each Email In emails
            Try

                SendHTMLEmail(Email.Email_To, "BC@LocalMunchiesDelivery.com", Email.Email_Subject, Email.Email_Body, Email.Email_Attachment_bln, Email.Email_Attachment_Path)
            Catch ex As Exception
                ' MsgBox(ex.Message)
                'SendEmail("bennyc66@gmail.com", "NoReply@localmunchies.com", "Error Sending Emails", ex.ToString, False, "")
                Dim erroremail As New EmailModel
                SendEmail(ITEmail, "NoReply@LocalMunchiesDelivery.com", "Error Sending Emails", Email.Email_To & vbCrLf & vbCrLf & ex.ToString, False, "")
                'erroremail.AddEmail("E", "1", "bennyc66@gmail.com", "NoReply@LocalMunchiesDelivery.com", "Error Sending Emails", Email.Email_To & vbCrLf & vbCrLf & ex.ToString, False, "", 1)
            End Try

            Email.Email_Status = 2
            Email.Email_Sent_Date = Now
            db.Entry(Email).State = EntityState.Modified
            db.SaveChanges()

            EmailSendVM.Message = "SENT"

        Next
        Return View(EmailSendVM)
    End Function
    Shared Function SendEmail(ByVal StrTo As String, ByVal StrFrom As String, ByVal StrSubject As String, ByVal StrBody As String, ByVal BlnAttachment As Boolean, ByVal StrAttachment As String) As Boolean
        'Send email notifictaion

        'Create the email objects
        Dim ObjEmail As New System.Net.Mail.SmtpClient
        Dim ObjEmailMsg As New System.Net.Mail.MailMessage(StrFrom, StrTo)
        Dim ObjAttachment As System.Net.Mail.Attachment
        'Dim htmlview As AlternateView = AlternateView.CreateAlternateViewFromString(Replace(StrBody, vbCr, "<br />"), Nothing, "text/html")

        'Initialize the email message ohject variables
        With ObjEmailMsg
            .Subject = StrSubject
            .Body = StrBody
        End With

        If BlnAttachment Then
            'Add File Attachment
            ObjAttachment = New System.Net.Mail.Attachment(StrAttachment)
            ObjEmailMsg.Attachments.Add(ObjAttachment)
        End If
        ' ObjEmailMsg.AlternateViews.Add(htmlview)

        'Initialize the SMTP server
        ObjEmail.Host = "mail.localmunchies.com"
        ObjEmail.Credentials = New Net.NetworkCredential(StrFrom, "")
        ObjEmail.Port = 587

        'Send email
        ObjEmail.Send(ObjEmailMsg)

        'Release the email object
        ObjEmail = Nothing
    End Function
    Shared Function SendHTMLEmail(ByVal StrTo As String, ByVal StrFrom As String, ByVal StrSubject As String, ByVal StrBody As String, ByVal BlnAttachment As Boolean, ByVal StrAttachment As String) As Boolean
        'Send email notifictaion

        'Create the email objects
        Dim ObjEmail As New System.Net.Mail.SmtpClient
        Dim ObjEmailMsg As New System.Net.Mail.MailMessage(StrFrom, StrTo)
        Dim ObjAttachment As System.Net.Mail.Attachment
        'Dim htmlview As AlternateView = AlternateView.CreateAlternateViewFromString(Replace(StrBody, vbCr, "<br />"), Nothing, "text/html")

        'Initialize the email message ohject variables
        With ObjEmailMsg
            .Subject = StrSubject
            .Body = StrBody
        End With

        ObjEmailMsg.IsBodyHtml = True


        If BlnAttachment Then
            'Add File Attachment
            ObjAttachment = New System.Net.Mail.Attachment(StrAttachment)
            ObjEmailMsg.Attachments.Add(ObjAttachment)
        End If
        ' ObjEmailMsg.AlternateViews.Add(htmlview)

        'Initialize the SMTP server
        'ObjEmail.Host = "mail.gemcomedical.com"  '(216.96.82.217)
        'ObjEmail.Host = "mail.localmunchies.com"
        'ObjEmail.Credentials = New Net.NetworkCredential(StrFrom, "munchies1")
        'ObjEmail.Port = 587

        'Send email
        ObjEmail.Send(ObjEmailMsg)

        'Release the email object
        ObjEmail = Nothing
    End Function
    Function AddCustomersServedEmail()
        Dim EmailSendVM As New EmailSendVM
        Try


            Dim Email As New EmailModel

            Dim SQL As String = ""
            Dim CS As New LM_LMD_CustomersServed
            CS.LM_CustomersServed = GetCustomersServed(System.Configuration.ConfigurationManager.ConnectionStrings("LMProd").ConnectionString, "select * from [LM_Customer_Report]")
            CS.LMD_CustomersServed = GetCustomersServed(System.Configuration.ConfigurationManager.ConnectionStrings("LMDProd").ConnectionString, "select * from [LMD_Customer_Report]")


            Dim emailto = "Bennyc66@gmail.com,sjones@localmunchies.com,nj5121@gmail.com"
            Dim emailfrom = "NoReply@LocalMunchiesDelivery.com"
            Dim emailsubject = "Customers Served"
            Dim Email_Body = Email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/EmailBody/CustomersServed.vbhtml", CS, "_Email.vbhtml")

            Email.AddEmail("A", 1, emailto, emailfrom, emailsubject, Email_Body, False, "", 1)

        Catch ex As Exception
            EmailSendVM.Message = ex.ToString
        End Try

        Return View(EmailSendVM)
    End Function
    Function GetCustomersServed(ByVal Connstr As String, ByVal Sql As String) As CustomersServedVM
        Dim CS As New CustomersServedVM
        Dim ObjConn As System.Data.SqlClient.SqlConnection
        Dim ObjCmd As System.Data.SqlClient.SqlCommand
        Dim ObjDR As System.Data.SqlClient.SqlDataReader

        'Create DB connection object
        ObjConn = New System.Data.SqlClient.SqlConnection(Connstr)

        'Open DB connection
        ObjConn.Open()

        'Create command Object
        ObjCmd = New System.Data.SqlClient.SqlCommand(Sql, ObjConn)
        ObjCmd.CommandTimeout = 900000

        'Create data reader object
        ObjDR = ObjCmd.ExecuteReader()

        'Get product prices
        If ObjDR.HasRows Then
            While ObjDR.Read()
                ' AddtoEmails(ObjDR.Item("emailconnstr").ToString, ObjDR.Item("emailquery").ToString, ObjDR.Item("emailtext").ToString, ObjDR.Item("emailsubject").ToString, ObjDR.Item("emailfrom").ToString, ObjDR.Item("id").ToString)
                CS.TotalCustomers = ObjDR.Item("TotalCustomers").ToString
                CS.TotalCustomersDay = ObjDR.Item("TotalCustomersDay").ToString
                CS.TotalCustomersWeek = ObjDR.Item("TotalCustomersWeek").ToString
                CS.AvgCustomersPerDayLastWeek = ObjDR.Item("AvgCustomersPerDayLastWeek").ToString
                CS.TotalCustomersMonth = ObjDR.Item("TotalCustomersMonth").ToString
                CS.AvgCustomersPerDayLastMonth = ObjDR.Item("AvgCustomersPerDayLastMonth").ToString
                CS.TotalCustomersYear = ObjDR.Item("TotalCustomersYear").ToString
                CS.AvgCustomersPerDayLastYear = ObjDR.Item("AvgCustomersPerDayLastYear").ToString


            End While
        End If
        ObjDR.Close()
        ObjCmd.Dispose()
        ObjConn.Dispose()
        Return CS
    End Function
    Function GetCustomersServedXML(ByVal WebAddress As String)
        Dim CS As New CustomersServedVM

        Dim contents = GetWebResponse(WebAddress)

        ' Create a new, empty XML document
        Dim document As New System.Xml.XmlDocument()

        ' Load the contents into the XML document
        document.LoadXml(contents)


        Dim xmlelement As System.Xml.XmlElement = document.FirstChild()

        CS.TotalCustomers = xmlelement.SelectSingleNode("TotalCustomers").InnerText
        CS.TotalCustomersDay = xmlelement.SelectSingleNode("TotalCustomersDay").InnerText
        CS.TotalCustomersWeek = xmlelement.SelectSingleNode("TotalCustomersWeek").InnerText
        CS.AvgCustomersPerDayLastWeek = xmlelement.SelectSingleNode("AvgCustomersPerDayLastWeek").InnerText
        CS.TotalCustomersMonth = xmlelement.SelectSingleNode("TotalCustomersMonth").InnerText
        CS.AvgCustomersPerDayLastMonth = xmlelement.SelectSingleNode("AvgCustomersPerDayLastMonth").InnerText
        CS.TotalCustomersYear = xmlelement.SelectSingleNode("TotalCustomersYear").InnerText
        CS.AvgCustomersPerDayLastYear = xmlelement.SelectSingleNode("AvgCustomersPerDayLastYear").InnerText

        Return CS
    End Function
    Function GetWebResponse(ByVal url As String) As String
        Dim Val As String = ""
        Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(url)



        ' Call the remote site, and parse the data in a response object
        Dim response As System.Net.HttpWebResponse = request.GetResponse()

        ' Check if the response is OK (status code 200)
        If response.StatusCode = System.Net.HttpStatusCode.OK Then

            ' Parse the contents from the response to a stream object
            Dim stream As System.IO.Stream = response.GetResponseStream()
            ' Create a reader for the stream object
            Dim reader As New System.IO.StreamReader(stream)
            ' Read from the stream object using the reader, put the contents in a string
            Val = reader.ReadToEnd()
        Else
            Throw New Exception("Could not retrieve document from the URL, response code: " & response.StatusCode)
        End If

        Return Val
    End Function
    Function CustomersServedXML()
        Dim XmlStr As String
        Dim Connstr = System.Configuration.ConfigurationManager.ConnectionStrings("LMDProd").ConnectionString
        Dim sql = "select * from [LMD_Customer_Report]"
        Dim ObjConn As System.Data.SqlClient.SqlConnection
        Dim ObjCmd As System.Data.SqlClient.SqlCommand
        Dim ObjDR As System.Data.SqlClient.SqlDataReader

        'Create DB connection object
        ObjConn = New System.Data.SqlClient.SqlConnection(Connstr)

        'Open DB connection
        ObjConn.Open()

        'Create command Object
        ObjCmd = New System.Data.SqlClient.SqlCommand(sql, ObjConn)
        ObjCmd.CommandTimeout = 900000

        'Create data reader object
        ObjDR = ObjCmd.ExecuteReader()

        'Get product prices
        If ObjDR.HasRows Then
            While ObjDR.Read()

                XmlStr = "<stats>"



                XmlStr &= "<TotalCustomers>" & ObjDR.Item("TotalCustomers").ToString & "</TotalCustomers>"
                XmlStr &= "<TotalCustomersDay>" & ObjDR.Item("TotalCustomersDay").ToString & "</TotalCustomersDay>"
                XmlStr &= "<TotalCustomersWeek>" & ObjDR.Item("TotalCustomersWeek").ToString & "</TotalCustomersWeek>"
                XmlStr &= "<AvgCustomersPerDayLastWeek>" & ObjDR.Item("AvgCustomersPerDayLastWeek").ToString & "</AvgCustomersPerDayLastWeek>"
                XmlStr &= "<TotalCustomersMonth>" & ObjDR.Item("TotalCustomersMonth").ToString & "</TotalCustomersMonth>"
                XmlStr &= "<AvgCustomersPerDayLastMonth>" & ObjDR.Item("AvgCustomersPerDayLastMonth").ToString & "</AvgCustomersPerDayLastMonth>"
                XmlStr &= "<TotalCustomersYear>" & ObjDR.Item("TotalCustomersYear").ToString & "</TotalCustomersYear>"
                XmlStr &= "<AvgCustomersPerDayLastYear>" & ObjDR.Item("AvgCustomersPerDayLastYear").ToString & "</AvgCustomersPerDayLastYear>"

                XmlStr &= "</stats>"

            End While
        End If
        ObjDR.Close()
        ObjCmd.Dispose()
        ObjConn.Dispose()

        Return Me.Content(XmlStr, "text/xml")
    End Function



    Function GetOpenJobs()
        Dim EmailSendVM As New EmailSendVM
        'Dim fromDate As DateTime = DateAdd(DateInterval.Minute, -2, Now.ToUniversalTime())

        Dim Jobs = db.Jobs.Where(Function(x) x.JobStatus = 1).ToList
        Dim Deliveries = db.Jobs.Where(Function(x) {2, 3}.Contains(x.JobStatus)).ToList

        If Jobs.Count > 0 And Deliveries.Count < 1 Then
            Dim Email As New EmailModel

            Dim emailto = ManagerEmail
            Dim emailfrom = "NoReply@LocalMunchiesDelivery.com"
            Dim emailsubject = "New Job"
            Dim Email_Body = Email.RenderViewToString(Me.ControllerContext.Controller, "~/Views/EmailBody/JobAlertEmail.vbhtml", Nothing, "_Email.vbhtml")

            Email.AddEmail("M", 2, emailto, emailfrom, emailsubject, Email_Body, False, "", 1)
        End If



        Return View(EmailSendVM)
    End Function

End Class