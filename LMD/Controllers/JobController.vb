﻿Imports System.Math
<Authorize()> _
Public Class JobController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Job
    Private db As LMDEntities = New LMDEntities


    Function Index() As ActionResult
        Dim Businesses = db.Businesses.Select(Function(x) x.ID).ToList
        Return View(Businesses)
    End Function

    Function Create() As ActionResult
        Dim Loggedinuser = UserModel.getUser(User.Identity.Name)

        Dim Offset = JobModel.getStartTimeOffset(Loggedinuser.BusinessID)
        Dim Starttime As DateTime = getCreateStartTime(5, Offset)
        ViewBag.StartTime = Starttime.ToString("h:mm tt")
        ViewBag.EndTime = DateAdd(DateInterval.Minute, 120, Starttime).ToString("h:mm tt")
        ViewBag.Times = Gettimes(5, 120, Starttime)
        Dim job As New Job
        Dim businessmodel As New BusinessModel
        Dim business As Business = businessmodel.GetBussinessByUserName(User.Identity.Name)
        If Not business Is Nothing Then
            Dim defaults As BusinessDefaults = businessmodel.GetDefaults(business.ID)
            If Not defaults Is Nothing Then
                job.DeliveryCity = defaults.Delivery_City
                job.DeliveryState = defaults.Delivery_State
                job.DeliveryZip = defaults.Delivery_Zip
            End If
        End If

        Return View(job)
    End Function
    Shared Function Gettimes(ByVal interval As Double, ByVal minuteduration As Integer, ByVal starttime As DateTime) As List(Of DateTime)
        Dim endtime As DateTime = DateAdd(DateInterval.Minute, minuteduration, starttime)
        Dim Retlst As New List(Of DateTime)


        While starttime < endtime
            Retlst.Add(starttime)
            starttime = DateAdd(DateInterval.Minute, interval, starttime)
        End While
        Return Retlst
    End Function
    Shared Function getCreateStartTime(ByVal NearestMinute As Integer, ByVal ScheduleBuffer As Integer) As DateTime
        Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))

        'Buffer Time
        EasternNow = DateAdd(DateInterval.Minute, ScheduleBuffer, EasternNow)

        Dim mins = NearestMinute * Round(Minute(EasternNow) / NearestMinute, 0)

        Dim Starttime As New DateTime(EasternNow.Year, EasternNow.Month, EasternNow.Day, EasternNow.Hour, 0, 0)
        Return DateAdd(DateInterval.Minute, mins, Starttime)
    End Function
    <HttpPost()> _
    Function Create(Model As Job) As ActionResult
        'Get Business
        Dim Loggedinuser = UserModel.getUser(User.Identity.Name)


        Model.BusinessID = Loggedinuser.BusinessID

        'Get User
        Model.EnterUserName = User.Identity.Name

        Model.RestaurntFee = 2.99

        'Default Dates
        Model.ActualDeliveryDate = #1/1/1900#
        Model.ActualPickupDate = #1/1/1900#
        Model.DeliveryDate = #1/1/1900#
        Model.VoidDate = #1/1/1900#
        Model.ExpectedArrivalTime = #1/1/1900#
        Model.CreateDate = Now.ToUniversalTime

        Dim Geocode As GeoCode = JobModel.AddressToGeocode(Model.DeliveryAddress & ", " & Model.DeliveryCity & ", " & Model.DeliveryState & ", " & Model.DeliveryZip)

        Model.DeliveryAddressLatitude = Geocode.Lat
        Model.DeliveryAddressLongitude = Geocode.Lon

        Model.DriverID = DriverModel.getDriverForBusiness(Loggedinuser.BusinessID)
        If Model.DriverID <> 0 Then
            'If on a driver Assign it
            Model.JobStatus = 2
        Else
            'No Driver Assigned Leave it open
            Model.JobStatus = 1
        End If

        Model.CreateDate = Now()

        If ModelState.IsValid Then
            db.Jobs.Add(Model)
            db.SaveChanges()
            Return RedirectToAction("Index", "BusinessConsole")
        End If

        Dim Offset = JobModel.getStartTimeOffset(Loggedinuser.BusinessID)
        Dim Starttime As DateTime = getCreateStartTime(5, Offset)
        ViewBag.StartTime = Starttime.ToString("h:mm tt")
        ViewBag.EndTime = DateAdd(DateInterval.Minute, 120, Starttime).ToString("h:mm tt")
        ViewBag.Times = Gettimes(5, 120, Starttime)
        Return View(Model)
    End Function

    Function Show(ByVal businessids As List(Of Integer), Optional ByVal statuses As String = "1,2,3,4,5,6") As ActionResult
        Dim statuslist = Split(statuses, ",").ToList
        Dim jobs = db.Jobs.Where(Function(x) businessids.Contains(x.BusinessID) And statuslist.Contains(x.JobStatus)
                                      ).ToList
        Return View(jobs)
    End Function

    Function ShowAdmin()
        Dim DateFrom = DateAdd(DateInterval.Day, -30, Now.Date())
        Dim jobs = db.Jobs.Where(Function(x) x.ScheduledPickupDate > DateFrom).ToList()
        Return View(jobs)
    End Function

    Function JobStatus(ByVal id As Integer)
        Dim statuses As New List(Of Status) From
            {
             New Status With {.id = 1, .Message = "Pending", .CssClass = "default"},
             New Status With {.id = 2, .Message = "Assigned", .CssClass = "primary"},
             New Status With {.id = 3, .Message = "Out For Delivery", .CssClass = "info"},
             New Status With {.id = 4, .Message = "Delivered", .CssClass = "success"},
             New Status With {.id = 5, .Message = "Voided - Business", .CssClass = "danger"},
             New Status With {.id = 6, .Message = "Voided - Driver", .CssClass = "danger"},
             New Status With {.id = 7, .Message = "Voided - Admin", .CssClass = "danger"}
            }


        Dim status = statuses.Where(Function(x) x.id = id).FirstOrDefault

        Return View(status)

    End Function
    Function Void(ByVal id As Integer)
        Dim job = db.Jobs.Find(id)
        Return View(job)
    End Function
    <HttpPost()> _
    Function Void(ByVal id As Integer, ByVal Reason As String) As ActionResult
        Dim job As New JobModel
        If job.Void(id, User.Identity.Name, Reason) Then
            Return RedirectToAction("Index", "Home")
        Else
            Return View()
        End If
    End Function
    Function Reset(ByVal id As Integer) As ActionResult
        Dim job = db.Jobs.Find(id)
        Return View(job)
    End Function

    Function ResetJob(ByVal jobid As Integer) As ActionResult
        Dim job As New DriverModel
        If job.ResetJob(jobid) Then
            Return RedirectToAction("Index", "Home")
        Else
            Return RedirectToAction("Reset", New With {.id = jobid})
        End If
    End Function


    Function Zoom(ByVal job As Job)
        Return View(job)
    End Function

    Function HeatMap() As ActionResult
        'Dim addresses = db.Jobs.Where(Function(x) x.JobStatus = 4).Select(Function(x) x.DeliveryAddress & " " & x.DeliveryCity & ", " & x.DeliveryState & " " & x.DeliveryZip).ToList
        Dim business = UserModel.getUser(User.Identity.Name).BusinessID

        Dim jobs = db.Jobs.Where(Function(x) x.JobStatus = 4 And
                                         x.DeliveryAddressLongitude <> 0 And
                                         x.DeliveryAddressLongitude <> -81.3578859 And
                                         x.DeliveryAddressLatitude <> 41.1536674
                                                 ).ToList
        If business <> 0 Then
            jobs = jobs.Where(Function(x) x.BusinessID = business)
        End If

        Dim geocodes = jobs.Select(Function(x) New GeoCode With {.Lon = x.DeliveryAddressLongitude, .Lat = x.DeliveryAddressLatitude}).ToList

        Return View(geocodes)
    End Function

End Class