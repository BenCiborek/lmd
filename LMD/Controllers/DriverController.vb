﻿Imports System.Data.Entity
Imports LMD
<Authorize()> _
Public Class DriverController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Driver

    Private db As LMDEntities = New LMDEntities

    Function Index() As ActionResult
        Return View()
    End Function

    Function Create() As ActionResult
        Return View()
    End Function

    <HttpPost()> _
    Function Create(Model As Driver) As ActionResult
        If ModelState.IsValid Then
            db.Drivers.Add(Model)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If


        Return View(Model)
    End Function

    Function Show() As ActionResult
        Dim Businesses = db.Drivers
        Return View(Businesses.ToList())
    End Function

    Function Edit(id As Integer) As ActionResult
        Dim Drivers = db.Drivers.Find(id)
        Return View(Drivers)
    End Function

    <HttpPost()> _
    Function Edit(Model As Driver) As ActionResult
        If ModelState.IsValid Then
            db.Entry(Model).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If


        Return View(Model)
    End Function

    Function getActiveDrivers() As ActionResult
        Dim Drivers = db.Drivers.Where(Function(x) x.Inactive = False).ToList
        Return View(Drivers)
    End Function

End Class