﻿Imports System.Data.Entity
Imports LMD
<Authorize()> _
Public Class BusinessController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Business

    Private db As LMDEntities = New LMDEntities

    Function Index() As ActionResult
        Return View()
    End Function

    Function Create() As ActionResult
        Return View()
    End Function

    <HttpPost()> _
    Function Create(Model As Business) As ActionResult
        If ModelState.IsValid Then
            db.Businesses.Add(Model)
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If

        Return View(Model)
    End Function

    Function Show() As ActionResult
        Dim Businesses = db.Businesses
        Return View(Businesses.ToList())
    End Function

    Function Edit(id As Integer) As ActionResult
        Dim business = db.Businesses.Find(id)
        Return View(business)
    End Function

    <HttpPost()> _
    Function Edit(Model As Business) As ActionResult
        If ModelState.IsValid Then
            db.Entry(Model).State = EntityState.Modified
            db.SaveChanges()
            Return RedirectToAction("Index")
        End If


        Return View(Model)
    End Function

    Function getActiveBusinesses() As ActionResult
        Dim Businesses = db.Businesses.Where(Function(x) x.Inactive = False).ToList
        Return View(Businesses)
    End Function

End Class