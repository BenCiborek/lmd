﻿Public Class HeaderController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /Header

    Function Index() As ActionResult
        Return View()
    End Function

    Function LogonLogoff() As ActionResult
        Dim LogonLogoffVM As New LogonLogoffVM
        LogonLogoffVM.IsLoggedIn = User.Identity.IsAuthenticated
        If User.Identity.IsAuthenticated Then
            LogonLogoffVM.UserEmail = UserModel.getUser(User.Identity.Name).Email
        Else
            LogonLogoffVM.UserEmail = ""
        End If

        Return View(LogonLogoffVM)
    End Function

End Class