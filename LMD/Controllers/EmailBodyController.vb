﻿Public Class EmailBodyController
    Inherits System.Web.Mvc.Controller

    '
    ' GET: /EmailBody

    Dim LMConnstr = ""
    Dim LMDConnstr = ""

    Function Index() As ActionResult
        Return View()
    End Function
    Function Header(Optional ByVal Message As String = "")
        Dim EmailHeaderVM As New EmailHeaderVM
        EmailHeaderVM.Message = Message
        Return View(EmailHeaderVM)
    End Function
    Function Footer() As ActionResult
        Return View()
    End Function

    Function CustomersServed() As ActionResult
        Dim SQL As String = ""
        Dim CS As New LM_LMD_CustomersServed
        CS.LM_CustomersServed = GetCustomersServed(System.Configuration.ConfigurationManager.ConnectionStrings("LMProd").ConnectionString, "select * from [LM_Customer_Report]")
        CS.LMD_CustomersServed = GetCustomersServed(System.Configuration.ConfigurationManager.ConnectionStrings("LMDProd").ConnectionString, "select * from [LMD_Customer_Report]")

        Return View(CS)
    End Function

    Function GetCustomersServed(ByVal Connstr As String, ByVal Sql As String) As CustomersServedVM
        Dim CS As New CustomersServedVM
        Dim ObjConn As System.Data.SqlClient.SqlConnection
        Dim ObjCmd As System.Data.SqlClient.SqlCommand
        Dim ObjDR As System.Data.SqlClient.SqlDataReader

        'Create DB connection object
        ObjConn = New System.Data.SqlClient.SqlConnection(Connstr)

        'Open DB connection
        ObjConn.Open()

        'Create command Object
        ObjCmd = New System.Data.SqlClient.SqlCommand(Sql, ObjConn)
        ObjCmd.CommandTimeout = 900000

        'Create data reader object
        ObjDR = ObjCmd.ExecuteReader()

        'Get product prices
        If ObjDR.HasRows Then
            While ObjDR.Read()
                ' AddtoEmails(ObjDR.Item("emailconnstr").ToString, ObjDR.Item("emailquery").ToString, ObjDR.Item("emailtext").ToString, ObjDR.Item("emailsubject").ToString, ObjDR.Item("emailfrom").ToString, ObjDR.Item("id").ToString)
                CS.TotalCustomers = ObjDR.Item("TotalCustomers").ToString
                CS.TotalCustomersDay = ObjDR.Item("TotalCustomersDay").ToString
                CS.TotalCustomersWeek = ObjDR.Item("TotalCustomersWeek").ToString
                CS.AvgCustomersPerDayLastWeek = ObjDR.Item("AvgCustomersPerDayLastWeek").ToString
                CS.TotalCustomersMonth = ObjDR.Item("TotalCustomersMonth").ToString
                CS.AvgCustomersPerDayLastMonth = ObjDR.Item("AvgCustomersPerDayLastMonth").ToString
                CS.TotalCustomersYear = ObjDR.Item("TotalCustomersYear").ToString
                CS.AvgCustomersPerDayLastYear = ObjDR.Item("AvgCustomersPerDayLastYear").ToString


            End While
        End If
        ObjDR.Close()
        ObjCmd.Dispose()
        ObjConn.Dispose()
        Return CS
    End Function
    Function JobAlertEmail()

        Return View()
    End Function

End Class