﻿Imports System.Data.Entity
Imports LMD
Namespace LMD
    <Authorize()> _
    Public Class DriverConsoleController
        Inherits System.Web.Mvc.Controller

        '
        ' GET: /DriverConsole
        Dim db As LMDEntities = New LMDEntities

        Function Index(Optional ByVal Message As String = "") As ActionResult
            Dim d As New DriverModel
            Dim logUser As User = UserModel.getUser(User.Identity.Name)
            Dim job = d.hasOpenJob(logUser.DriverID)
            Dim DriverConsoleVM As New DriverConsoleVM

            DriverConsoleVM.Message = Message
            DriverConsoleVM.hasAlert = d.hasAlert(logUser.DriverID)

            If Not job Is Nothing Then
                Select Case job.JobStatus
                    Case 2
                        Return RedirectToAction("Pickup", New With {.id = job.ID})
                    Case 3
                        Return RedirectToAction("Delivery", New With {.id = job.ID})
                End Select
            End If
            Return View(DriverConsoleVM)
        End Function

        Function getNextJob(Optional ByVal lat As Double = 0, Optional ByVal lon As Double = 0)
            Dim d As New DriverModel

            'If the driver does not have an open Job
            If d.hasOpenJob(UserModel.getUser(User.Identity.Name).DriverID) Is Nothing Then
                Dim Job = d.getNextJob()
                If Job Is Nothing Then
                    Return RedirectToAction("Index", New With {.Message = "No New Jobs. Check Back Soon."})
                Else
                    'Dim allJobsForBusiness = db.Jobs.Where(Function(x) x.JobStatus = 1 And x.BusinessID = Job.BusinessID)
                    'For Each commonJob In allJobsForBusiness
                    '    Dim ErrorMessage As ErrorMessage = d.takeJob(commonJob.ID, UserModel.getUser(User.Identity.Name).DriverID)
                    '    If ErrorMessage.isError = False Then
                    '        'Return RedirectToAction("Index", New With {.Message = ErrorMessage.Message})
                    '    End If
                    'Next
                    d.takeAllJobs(UserModel.getUser(User.Identity.Name).DriverID, Job.BusinessID, lat, lon)
                    Return RedirectToAction("Pickup", New With {.id = Job.ID})
                End If
            Else
                Return RedirectToAction("Index")
            End If




        End Function

        Function Pickup(ByVal id As Integer)
            Dim d As New DriverModel


            Dim job = d.getPickup(id, UserModel.getUser(User.Identity.Name.ToString).DriverID)
            If job Is Nothing Then
                Return RedirectToAction("Index")
            Else
                Return View(job)
            End If


        End Function

        Function OrderPickedUp(ByVal ID As Integer)
            Dim d As New DriverModel


            If d.jobPickedUp(ID, UserModel.getUser(User.Identity.Name.ToString).DriverID) Then
                Return RedirectToAction("Delivery", New With {.id = ID})
            Else
                Return RedirectToAction("Index")
            End If
        End Function

        Function Delivery(ByVal id As Integer)
            Dim d As New DriverModel

            Dim DeliveryVM As New DeliveryVM
            DeliveryVM.JobID = id
            DeliveryVM.Job = d.getDelivery(id, UserModel.getUser(User.Identity.Name.ToString).DriverID)
            If DeliveryVM.Job Is Nothing Then
                Return RedirectToAction("Index")
            Else
                Return View(DeliveryVM)
            End If
        End Function

        Function OrderDelivered(ByVal Model As DeliveryVM)
            Dim d As New DriverModel



            If ModelState.IsValid Then

                If d.jobDelivered(Model.JobID, UserModel.getUser(User.Identity.Name.ToString).DriverID, Model.MoneyCollected) Then
                    Return RedirectToAction("Index")
                Else
                    Return RedirectToAction("Index")
                End If
            End If
            Return RedirectToAction("Delivery", New With {.id = Model.JobID})
        End Function

        Function OtherDeliveries(ByVal id As Integer)
            Dim d As New DriverModel
            Dim job = d.getPickup(id, UserModel.getUser(User.Identity.Name.ToString).DriverID)

            If job Is Nothing Then
                Return RedirectToAction("Index")
            End If
            Dim jobs = d.getOtherPickups(job)

            If jobs Is Nothing Then
                Return RedirectToAction("Index")
            Else
                Return View(jobs)
            End If
        End Function

        Function OtherDeliverySummary(ByVal Deliveries As String) As ActionResult
            Dim selectedDeliveries As List(Of String) = Split(Deliveries, ",").ToList
            Dim DriverID = UserModel.getUser(User.Identity.Name.ToString).DriverID
            If Deliveries <> "" Then
                Dim jobs = db.Jobs.Where(Function(x) selectedDeliveries.Contains(x.ID) And x.JobStatus = 2 And x.DriverID = DriverID).ToList
                If jobs.Count > 0 Then
                    Return View(jobs)
                End If
                Return RedirectToAction("Index")
            Else
                Return RedirectToAction("Index")
            End If

        End Function

        Function StartDelivering(ByVal jobids As String)
            Dim selectedDeliveries As List(Of String) = Split(jobids, ",").ToList
            Dim jobs = db.Jobs.Where(Function(x) selectedDeliveries.Contains(x.ID)).ToList()
            Dim d As New DriverModel
            For Each Job In jobs
                d.jobPickedUp(Job.ID, UserModel.getUser(User.Identity.Name.ToString).DriverID)

            Next
            d.releaseJobs(UserModel.getUser(User.Identity.Name.ToString).DriverID, 2)
            Try
                d.AssignDeliveryPriority(UserModel.getUser(User.Identity.Name.ToString).DriverID)
            Catch ex As Exception

            End Try

            Dim nextDelivery = DriverModel.isThereAPickupReady(UserModel.getUser(User.Identity.Name.ToString).DriverID)

            If Not nextDelivery Is Nothing Then
                If DriverModel.readyForAnotherPickup(DriverModel.currentJobs(UserModel.getUser(User.Identity.Name.ToString).DriverID)) Then
                    'The Driver Can Handle another Pickup
                    d.takeAllJobs(UserModel.getUser(User.Identity.Name).DriverID, nextDelivery.BusinessID)
                    Return RedirectToAction("Index")
                Else
                    'Driver Cannot Take another pickup
                    Return RedirectToAction("Index")
                End If
            Else
                'No More Deliveries
                Return RedirectToAction("Index")
            End If



        End Function

        Function showOpenJobs()
            Dim d As New DriverModel
            Dim jobs = d.getOpenJobs()
            Return View(jobs)
        End Function

        Function EndofDay(ByVal rundate As String)
            Dim d As New DriverModel


            Dim DriverCash As List(Of DriverCashVM) = d.DriverCashoutReport(rundate, UserModel.getUser(User.Identity.Name).DriverID)

            ViewBag.rundate = rundate

            Return View(DriverCash.FirstOrDefault())
        End Function

        Function CheckIn() As ActionResult
            Dim Model As New DriverTime
            Dim starttime = JobController.getCreateStartTime(30, 30)
            ViewBag.Times = JobController.Gettimes(30, 600, starttime)

            Model.StartTime = Now().ToUniversalTime
            Dim driver As Driver = DriverModel.getDriverByUserName(User.Identity.Name)
            Dim thisuser As User = UserModel.getUser(User.Identity.Name)
            Model.DriverID = driver.ID
            Model.Phone = driver.Phone
            Model.Email = thisuser.Email


            Return View(Model)
        End Function

        <HttpPost()> _
        Function CheckIn(ByVal model As DriverTime)
            model.StartTime = Now.ToUniversalTime
            model.LastAlerted = #1/1/1900#
            model.EndTime = System.TimeZoneInfo.ConvertTimeToUtc(model.EndTime, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
            If ModelState.IsValid Then
                db.DriverTimes.Add(model)
                db.SaveChanges()
                Return RedirectToAction("Index")
            End If

            Dim starttime = JobController.getCreateStartTime(30, 30)
            ViewBag.Times = JobController.Gettimes(30, 600, starttime)
            Return View(model)
        End Function

    End Class
End Namespace
