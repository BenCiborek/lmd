﻿@Modeltype List(Of LMD.Job)
@code
    ViewData("Title") = "| Money Report"
End Code
@Html.Action("AdminNav", "Admin")
<table class="table">
    <thead>
        <tr>
            @*Date/Business/Delivery Address/Money To Collect/Money Collected*@
            <th>Scheduled Date</th>
            <th>Business Number</th>
            <th>Status</th>
            <th>Business</th>
            <th>Delivery Name</th>
            <th>Delivery Address</th>
            <th>Driver</th>
            <th>Collection Type</th>
            <th>Tip Collected</th>
            <th>Money To Collect</th>
            <th>Money Collected</th>
            <th>Fee</th>
        </tr>
    </thead>
    <tbody>
        @For Each Jobs In Model.OrderByDescending(Function(o) o.ScheduledPickupDate)
            @<tr>
                <td>@Jobs.ScheduledPickupDate</td>
                <td>@Jobs.DeliveryNumber</td>
                <td>@Jobs.JobStatus</td>
                <td>@Jobs.Business.Name</td>
                <td>@Jobs.DeliveryName</td>
                <td>@Jobs.DeliveryAddress</td>
                <td>@Jobs.DriverID</td>
                <td>@Jobs.DeliveryType</td>
                <td>@Jobs.TipCollected</td>
                <td>@Jobs.MoneyToCollect</td>
                <td>@Jobs.MoneyCollected</td>
                <td>@Jobs.RestaurntFee</td>
        </tr>
        Next
    </tbody>
</table>