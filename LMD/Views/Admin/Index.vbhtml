﻿@Code
    ViewData("Title") = "| Admin"
    Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
End Code

@Html.Action("AdminStats", New With {.dt = EasternNow.Date()})

@Html.Action("AdminNav", "Admin")

@Html.Action("ShowAlert", "Home", New With {.Type = "Admin"})

@Html.Action("ShowAdmin", "Job")
