﻿@Modeltype List(Of LMD.AdminBussinessSummaryReportVM)
@Code
    ViewData("Title") = " | Admin Console | Business Report"
    Dim startdate = Now.Date
    Dim enddate = DateAdd(DateInterval.Day, -150, Now.Date())
    Dim sStartdate = startdate
    Dim sEndDate = enddate
    
    'Adjust end date to 11:59 PM
    startdate = DateAdd(DateInterval.Hour, 23, startdate)
    startdate = DateAdd(DateInterval.Minute, 59, startdate)
End Code


<h2>Business Report</h2>
@Html.Action("AdminNav", "Admin")
<div class="row">
    <div class="col-xs-3">
        @*<a class="btn btn-default" href="@Url.Action("Index")">< Back</a>*@
    </div>
    <form>
        <div class="col-xs-3">
            <select id="startdate" name="startdate" class="form-control">
                @While sStartdate > sEndDate
                    @<option value="@sStartdate" @IIf(sStartdate = ViewBag.startdate, "selected", "")>@sStartdate</option>
                    sStartdate = DateAdd(DateInterval.Day, -1, sStartdate)
                End While
            </select>
        </div>
        <div class="col-xs-3">
            <select id="rundate" name="rundate" class="form-control">
                @While startdate > enddate
                    @<option value="@startdate" @IIf(startdate = ViewBag.rundate, "selected", "")>@startdate</option>
                    startdate = DateAdd(DateInterval.Day, -1, startdate)
                End While
            </select>
        </div>
        <div class="col-xs-3">
            <button type="submit" class="btn btn-primary">Update Report</button>
        </div>
    </form>
</div>


<table class="table">
    <thead>
        <tr>
            <th>Business</th>
            <th>Jobs</th>
            <th>Cash Collected</th>
            <th>Fees</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @For Each Business In Model
            @<tr>
                <td>@Business.Business.Name</td>
                <td>@Business.Jobs</td>
                <td>@FormatCurrency(Business.CashCollected)</td>
                <td>@FormatCurrency(Business.FeesDue)</td>
                <td>@FormatCurrency(Business.CashCollected - Business.FeesDue)</td>
            </tr>
        Next
    </tbody>
</table>
