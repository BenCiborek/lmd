﻿@ModelType LMD.AdminStatsVM
@Code
    Layout = Nothing
End Code
<div class="row">
    <div class="col-xs-3">
        <div class="well well-sm">
            <div class="text-center">
                <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span><br />
                @Model.AvgPickupTime<br />
                @Model.MaxPickupTime
            </div>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="well well-sm">
            <div class="text-center">
                <span class="glyphicon glyphicon-send" aria-hidden="true"></span><br />
                @Model.AvgTimeInCar<br />
                @Model.MaxTimeInCar
            </div>
        </div>

    </div>
    <div class="col-xs-3">
        <div class="well well-sm">
            <div class="text-center">
                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span><br />
                @Model.AvgTimeFromEntryToDelivery<br />
                @Model.MaxTimeFromEntryToDelivery
            </div>
        </div>

    </div>
    <div class="col-xs-3">
        <div class="well well-sm">
            <div class="text-center">
                <span class="glyphicon glyphicon-dashboard" aria-hidden="true"></span><br />
                @Html.Action("CurrentSetting")
            </div>
        </div>

    </div>
   
</div>
