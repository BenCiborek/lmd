﻿@Code
    ViewData("Title") = "| Admin | Alert Messages"
End Code

<h2>Alert Message</h2>

<br />

@*Create Alert*@
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        <a href="@Url.Action("AddAlertMessage", "Admin")" class="btn btn-block btn-primary">Create Alert!</a>
    </div>
</div>

@*Alert List*@
@Html.Action("showAlertMessages", "Admin")


