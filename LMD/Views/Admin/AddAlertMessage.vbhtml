﻿@ModelType LMD.AlertMessage
@Code
    ViewData("Title") = "| Admin | Add Alert Message"
End Code

<h2>New Alert</h2>

<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        @Using Html.BeginForm()
            @<div>
                <div>
                    @Html.ValidationSummary(True)
                </div>
                 <div class="form-group editor-field">
                     @Html.LabelFor(Function(model) model.Type)
                     <select id="Type" name="Type" class="form-control">
                         <option value="">Select A Type</option>
                         <option value="Business">Business</option>
                         <option value="Driver">Driver</option>
                         <option value="Admin">Admin</option>
                     </select>
                     @Html.ValidationMessageFor(Function(model) model.Type)
                 </div>
                 <div class="form-group editor-field">
                     @Html.LabelFor(Function(model) model.CssClass)
                     <select id="CssClass" name="CssClass" class="form-control">
                         <option value="">Select A Class</option>
                         <option value="info" class="info">Blue</option>
                         <option value="warning" class="warning">Yellow</option>
                         <option value="success" class="success">Green</option>
                         <option value="danger" class="danger">Red</option>
                     </select>
                     @Html.ValidationMessageFor(Function(model) model.Type)
                 </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.Message)
                    @Html.EditorFor(Function(model) model.Message)
                    @Html.ValidationMessageFor(Function(model) model.Message)
                </div>
                
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.Startdate)
                    @Html.EditorFor(Function(model) model.Startdate)
                    @Html.ValidationMessageFor(Function(model) model.Startdate)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.EndDate)
                    @Html.EditorFor(Function(model) model.EndDate)
                    @Html.ValidationMessageFor(Function(model) model.EndDate)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.Active)
                    @Html.EditorFor(Function(model) model.Active)
                    @Html.ValidationMessageFor(Function(model) model.Active)
                </div>
            </div>

            @<button class="btn btn-primary btn-block" style="padding-top:10px;">
                Submit
            </button>
        End Using
    </div>
</div>

<script src="/Content/FixForm.js"></script>
<script src="~/Content/dateTimeEntry/jquery.plugin.min.js"></script>
<script src="~/Content/dateTimeEntry/jquery.datetimeentry.min.js"></script>
<link rel="stylesheet" href="~/Content/dateTimeEntry/jquery.datetimeentry.css" />


<script>
    $('#Startdate').datetimeEntry();
    $('#EndDate').datetimeEntry();
</script>