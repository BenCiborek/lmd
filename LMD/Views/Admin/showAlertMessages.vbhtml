﻿@modeltype List(Of LMD.AlertMessage)
@Code
    Layout = Nothing
    Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
End Code

<table class="table">
    <thead>
        <tr>
            <th>
                Type
            </th>
            <th>
                Message
            </th>
            <th>
                Start Date
            </th>
            <th>
                End Date
            </th>
            <th>
                Active
            </th>
        </tr>
    </thead>
    <tbody>
        @For Each alert In Model.OrderByDescending(Function(x) x.EndDate).ThenByDescending(Function(x) x.Startdate)
            @<tr class="@IIf(alert.Active = True And alert.Startdate < EasternNow And alert.EndDate > EasternNow, alert.CssClass, "")">
                <td>
                    @alert.Type
                </td>
                <td>
                    @alert.Message
                </td>
                <td>
                    @alert.Startdate
                </td>
                <td>
                    @alert.EndDate
                </td>
                <td>
                    @alert.Active
                </td>
                <td>
                    @Html.ActionLink("Edit", "EditAlertMessage", New With {.id = alert.Id})
                </td>
            </tr>
        Next
    </tbody>
</table>
