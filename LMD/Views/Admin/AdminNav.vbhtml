﻿
@Code
    Layout = Nothing
End Code

<ul class="nav nav-pills">
    <li role="presentation"><a href="/">Admin - Home</a></li>
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Setup <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a class="" href="@Url.Action("Index", "Driver")">Drivers</a></li>
            <li><a class="" href="@Url.Action("Index", "Business")">Business</a></li>
            <li><a class="" href="@Url.Action("Index", "Account")">Users</a></li>
            <li><a class="" href="@Url.Action("AlertMessage")">Alerts</a></li>
        </ul>
    </li>
    <li role="presentation" class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            Reports <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li><a class="" href="@Url.Action("AdminMoney")">Money Collected Report</a></li>
            <li><a class="" href="@Url.Action("CashReport", New With {.rundate = Now.Date.ToString, .startdate = DateAdd(DateInterval.Day, -7, Now.Date).ToString})">Envelope Report</a></li>
            <li><a class="" href="@Url.Action("BusinessReport", New With {.rundate = Now.Date.ToString, .startdate = DateAdd(DateInterval.Day, -7, Now.Date).ToString})">Business Report</a></li>
        </ul>
    </li>
</ul>
