﻿@Modeltype list(of Lmd.DriverCashVM)
@Code
    ViewData("Title") = " | Admin Console | Cash Report"
    Dim startdate = Now.Date
    Dim enddate = DateAdd(DateInterval.Day, -30, Now.Date())
    Dim sStartdate = startdate
    Dim sEndDate = enddate
End Code

<h2>Cash Report</h2>
@Html.Action("AdminNav", "Admin")
<div class="row">
    <div class="col-xs-3">
        @*<a class="btn btn-default" href="@Url.Action("Index")">< Back</a>*@
    </div>
    <form>
        <div class="col-xs-3">
            <select id="startdate" name="startdate" class="form-control">
                @While sStartdate > sEndDate
                    @<option value="@sStartdate" @IIf(sStartdate = ViewBag.startdate, "selected", "")>@sStartdate</option>
                    sStartdate = DateAdd(DateInterval.Day, -1, sStartdate)
                End While
            </select>
        </div>
        <div class="col-xs-3">
            <select id="rundate" name="rundate" class="form-control">
                @While startdate > enddate
                    @<option value="@startdate" @IIf(startdate = ViewBag.rundate, "selected", "")>@startdate</option>
                    startdate = DateAdd(DateInterval.Day, -1, startdate)
                End While
            </select>
        </div>
        <div class="col-xs-3">
            <button type="submit" class="btn btn-primary">Update Report</button>
        </div>
    </form>
</div>


<table class="table">
    <thead>
        <tr>
            @*<th>Date</th>*@
            <th>Driver</th>
            <th>Jobs</th>
            <th>Envelope Amount</th>
            <th>CC Tips</th>
            <th>Cash Tips</th>
            <th>Delivery Fees</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @For Each Driver In Model.OrderBy(Function(o) o.DeliveryDate).ThenBy(Function(x) x.DriverID)
            @<tr>
                @*<td>@Driver.DeliveryDate</td>*@
                <td>@Driver.DriverID</td>
                <td>@Driver.Jobs</td>
                <td>@FormatCurrency(Driver.MoneyToCollect)</td>
                <td>@FormatCurrency(Driver.TipCollected)</td>
                <td>@FormatCurrency(Driver.MoneyCollected - Driver.MoneyToCollect)</td>
                <td>@FormatCurrency(Driver.Jobs * 2.0)</td>
                <td>
                    @FormatCurrency((Driver.Jobs * 2.0) + Driver.TipCollected)
                </td>
            </tr>
        Next
    </tbody>
</table>