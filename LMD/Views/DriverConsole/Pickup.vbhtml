﻿@Modeltype LMD.Job
@Code
    ViewData("Title") = "| Pickup"
    Dim AddressSearchString As String = Model.Business.Address1 & "+" & Model.Business.Address2 & "+" & Model.Business.City & "+" & Model.Business.State & "+" & Model.Business.Zip
End Code



<div class="panel  panel-info">
    <div class="panel-heading">Pickup</div>
    <div class="panel-body">

        <h3>@Model.Business.Name</h3>

        <h3>Order Number: @Model.DeliveryNumber</h3>

        <h3>@Model.ScheduledPickupDate</h3>

        <h3>@Model.DeliveryType</h3>

        <a href="https://www.google.com/maps/dir/Current+Location/@AddressSearchString">
            @Model.Business.Address1<br />
            @Model.Business.Address2<br />
            @Model.Business.City, @Model.Business.State @Model.Business.Zip
        </a>

    </div>
    <div class="panel-footer">
        <a class="btn btn-block btn-success" href="@Url.Action("OtherDeliveries", New With {.id = Model.ID})">Order Picked Up</a>
    </div>
</div>
@Html.Partial("~/Views/Shared/_autoRefresh.vbhtml")

@*<h2>Pickup</h2>

<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        <div class="well">
            <h3>@Model.Business.Name</h3>

            <h3>Order Number: @Model.DeliveryNumber</h3>

            <h3>@Model.ScheduledPickupDate</h3>

            <h3>@Model.DeliveryType</h3>

            <a href="https://www.google.com/maps/dir/Current+Location/@AddressSearchString">
                @Model.Business.Address1<br />
                @Model.Business.Address2<br />
                @Model.Business.City, @Model.Business.State @Model.Business.Zip
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        <a class="btn btn-block btn-success" href="@Url.Action("OtherDeliveries", New With {.id = Model.ID})">Order Picked Up</a>
    </div>
</div>*@
