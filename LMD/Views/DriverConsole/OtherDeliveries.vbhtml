﻿@Modeltype list(of LMD.Job)
@Code
    ViewData("Title") = "| Other Deliveries"
End Code


<div class="panel  panel-info">
    <div class="panel-heading">Are these orders ready?</div>
    <div class="panel-body">
        <div class="alert alert-danger hidden" id="alert" role="alert"></div>
        <table class="table">
            @For Each job In Model
                @<tr>
                    <td>
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-danger delivery" data-job="@job.ID">
                                <input type="checkbox" name="Deliveries" class="deliverybox" value="@job.ID" autocomplete="off" /> <div>Not Ready</div>
                            </label>
                        </div>
                    </td>
                    <td> @job.DeliveryNumber</td>
                    <td>
                        @job.DeliveryName<br />
                        @job.DeliveryAddress
                    </td>
                    <td>
                        @job.NotesToDriver
                    </td>
                </tr>
            Next
        </table>
    </div>
    <div class="panel-footer">
        <button class="btn btn-block btn-primary" id="ordersPickedUp" type="submit">Orders Picked Up</button>
    </div>
</div>

<script>

    $('#ordersPickedUp').on('click', function () {
        var str = $('.deliverybox:checked').map(function () { return this.value; }).get().join(',')

        if (str != '')
        {
            var url = '@Url.Action("OtherDeliverySummary", New With {.Deliveries = "__str__"})';
            window.location.href = url.replace('__str__', str);
        }
        else
        {
            //Show Alert
            $('#alert').text('Select a Delivery')
            $('#alert').removeClass('hidden')
        }


    })

    $('.delivery').on('click', function () {
        //alert('Here')
        $(this).toggleClass('btn-success') // button text will be "finished!"
        $(this).toggleClass('btn-danger')

        if ($(this).hasClass('btn-danger'))
        {
            $(this).find('div').text('Not Ready')
        }
        else
        {
            $(this).find('div').text('Picked Up')
        }
        
    })
</script>

@Html.Partial("~/Views/Shared/_autoRefresh.vbhtml")