﻿@Modeltype lmd.DriverCashVM
@Code
    ViewData("Title") = " | Driver | Envelope Report"
    Dim startdate = Now.Date
    Dim enddate = DateAdd(DateInterval.Day, -30, Now.Date())
End Code

<h2>Envelope Report <small>@ViewBag.rundate</small></h2>
<div class="row">
    <div class="col-xs-3">
        <a class="btn btn-default" href="@Url.Action("Index")">< Back</a>
    </div>
    <form>
        <div class="col-xs-4 col-sm-3" >

        </div>
        <div class="col-sm-3 col-xs-9">
            <select id="rundate" name="rundate" class="form-control">
                @While startdate > enddate
                    @<option value="@startdate" @IIf(startdate = ViewBag.rundate, "selected", "")>@startdate.ToString("MM/dd/yyyy")</option>
                    startdate = DateAdd(DateInterval.Day, -1, startdate)
                End While
            </select>
        </div>
        <div class="col-xs-3">
            <button type="submit" class="btn btn-primary">Update Report</button>
        </div>
    </form>
</div>


@If Model Is Nothing Then
    @<h2>
    No Deliveries for this date.
    </h2>
Else
    @<div>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2" style="text-align:center;">
            <h2>@FormatCurrency(Model.MoneyToCollect)</h2>
        </div>  
    </div>
         <div class="row">
             <div class="col-xs-12 col-sm-8 col-sm-offset-2" style="text-align:center;">
                 <table class="table">
                     @*<tr>
                         <td>Cash Collected:</td>
                         <td>@FormatCurrency(Model.MoneyCollected)</td>
                     </tr>
                     <tr>
                         <td>Cash Tips:</td>
                         <td>@FormatCurrency(Model.MoneyCollected - Model.MoneyToCollect)</td>
                     </tr>
                     <tr>
                         <td>CC Tips:</td>
                         <td>@FormatCurrency(Model.TipCollected)</td>
                     </tr>*@
                     <tr>
                         <td>Deliveries:</td>
                         <td>@Model.Jobs</td>
                     </tr>
                 </table>
             </div>
         </div>
    
    </div>
End If