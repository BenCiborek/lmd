﻿@Modeltype list(of LMD.Job)
@Code
    ViewData("Title") = ""
    Layout = Nothing
End Code

<table class="table">
    <thead>
        <tr>
            <th>Scheduled Date</th>
            <th>Business</th>
            <th>Status</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
        @For Each Jobs In Model.OrderBy(Function(o) o.ScheduledPickupDate)
            @<tr>
                <td>@Jobs.ScheduledPickupDate</td>
                <td>@Jobs.Business.Name</td>
                <td>@Jobs.JobStatus</td>
                <td>
                    @Jobs.DeliveryName<br />
                    @Jobs.DeliveryAddress<br />
                </td>
            </tr>
        Next
    </tbody>
</table>