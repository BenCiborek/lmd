﻿@Modeltype LMD.DriverTime
@Code
    ViewData("Title") = "| Driver Console | Check-In"
    Dim times As List(Of DateTime) = ViewBag.Times
    Dim currentselectedtime As DateTime
    If Not Model Is Nothing Then
        currentselectedtime = Model.EndTime
    End If
End Code

<h2>Driver Check-In</h2>

<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        @Using Html.BeginForm()
            @<div>
                <div>
                    @Html.ValidationSummary(True)
                </div>
                <div class="form-group editor-field">
                    <select id="EndTime" name="EndTime" class="form-control">
                        <option value="">End Time</option>
                        @For Each time In times
                            @<option value="@time" @IIf(currentselectedtime = time, "Selected", "")>@time</option>
                        Next
                    </select>
                    @Html.ValidationMessageFor(Function(model) model.EndTime)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.Type)
                    <select id="Type" name="Type" class="form-control">
                        <option value="">Select An Alert Method</option>
                        <option value="1">Email</option>
                        <option value="2">Text</option>
                    </select>
                    @Html.ValidationMessageFor(Function(model) model.Type)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.Email)
                    @Html.EditorFor(Function(model) model.Email)
                    @Html.ValidationMessageFor(Function(model) model.Email)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.Phone)
                    @Html.EditorFor(Function(model) model.Phone)
                    @Html.ValidationMessageFor(Function(model) model.Phone)
                </div>
            </div>
            @Html.HiddenFor(Function(model) model.StartTime)
            @Html.HiddenFor(Function(model) model.DriverID)
            @<button class="btn btn-primary btn-block" style="padding-top:10px;">
                Submit
            </button>
        End Using
    </div>
</div>

<script src="/Content/FixForm.js"></script>
