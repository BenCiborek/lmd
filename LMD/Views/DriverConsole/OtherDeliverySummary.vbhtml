﻿@Modeltype List(Of LMD.Job)
@Code
    ViewData("Title") = "| Other Deliveries Summary"
    Dim strDeliveries As String = ""
    For Each j In Model
        strDeliveries &= j.ID & ","
    Next
    Dim firstid = Model.FirstOrDefault().ID
End Code


<div class="panel  panel-info">
    <div class="panel-heading">Do you have these orders?</div>
    <div class="panel-body">
        <table class="table">
            @For Each job In Model
                @<tr>
                    <td> @job.DeliveryNumber</td>
                    <td>
                        @job.DeliveryName<br />
                        @job.DeliveryAddress
                    </td>
                </tr>
            Next
        </table>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-2 col-xs-6">
                <a href='@Url.Action("Otherdeliveries", New With {.id = firstid})' class="btn btn-default btn-block">< Back</a>
            </div>
            <div class="col-sm-4 col-xs-6">
                <a class="btn btn-block btn-success" href="@Url.Action("StartDelivering", New With{.jobids = strDeliveries} )">Start Delivering</a>
            </div>
        </div>
    </div>
</div>
@Html.Partial("~/Views/Shared/_autoRefresh.vbhtml")