﻿@Modeltype lmd.DriverConsoleVM
@Code
    ViewData("Title") = "| Driver Console"
    Dim NowDate = DateAdd(DateInterval.Hour, -4, Now.ToUniversalTime)
End Code

<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        @Using Html.BeginForm("getNextJob", "DriverConsole")
            @Html.Partial("~/Views/DriverConsole/_GetLocation.vbhtml")
            @<button type="submit" class="btn btn-block btn-primary">Get Next Job!</button>
        End Using
    </div>
</div>

@Html.Action("ShowAlert", "Home", New With {.Type = "Driver"})


@If ViewBag.Message <> "" Then
    

@<div class="row" style="padding-top:5px;">
    <div class="col-xs-12">
        <div class="alert alert-danger" role="alert">
            <p>@ViewBag.Message</p>
        </div>
    </div>
</div>
End If

@Html.Action("showOpenJobs")

<div class="row" style="padding-top:100px;">
    <div class="col-sm-4 col-sm-offset-2 col-xs-6">
        <a href="@Url.Action("EndofDay", New With {.rundate = NowDate.Date.ToString("MM/dd/yyyy")})" class="btn btn-block btn-default">Envelope Report</a>
    </div>
    <div class="col-sm-4 col-xs-6">
        @If Not Model.hasAlert Then
             @<a href="@Url.Action("CheckIn")" class="btn btn-block btn-success">Check-In</a>
        End If
    </div>
</div>


@Html.Partial("~/Views/Shared/_autoRefresh.vbhtml")
