﻿@Modeltype LMD.DeliveryVM
@Code
    ViewData("Title") = "| Delivery"
    Dim AddressSearchString As String = Model.Job.DeliveryAddress & "+" & Model.Job.DeliveryAddress2 & "+" & Model.Job.DeliveryCity & "+" & Model.Job.DeliveryState & "+" & Model.Job.DeliveryZip
    
End Code


<div class="panel  panel-success">
    <div class="panel-heading">Delivery</div>
    <div class="panel-body">
        <h3>@Model.Job.Business.Name - @Model.Job.DeliveryNumber</h3>
        <h3>@Model.Job.DeliveryName</h3>
        <h3><a href="tel:+1 @Model.Job.DeliveryPhone">@Model.Job.DeliveryPhone</a></h3>
        <h3>@Model.Job.DeliveryType</h3>
        <a href="https://www.google.com/maps/dir/Current+Location/@AddressSearchString">
            @Model.Job.DeliveryAddress<br />
            @Model.Job.DeliveryAddress2<br />
            @Model.Job.DeliveryCity, @Model.Job.DeliveryState @Model.Job.DeliveryZip
        </a>
        <h3>Notes: </h3>
        <div class="well">
            <p>@Model.Job.NotesToDriver</p>
        </div>
        <h3>@FormatCurrency(IIf(Model.Job.DeliveryType = "Collect Cash", Model.Job.MoneyToCollect, 0))</h3>

    </div>
    <div class="panel-footer">
      @Using Html.BeginForm("OrderDelivered", "DriverConsole")
    @<div>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 col-xs-12">
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.MoneyCollected)
                    <input name="MoneyCollected" class="text-box single-line form-control" step="any" style="width:100%" id="MoneyCollected" type="number" value="0" data-val-required="The MoneyCollected field is required." data-val-number="The field MoneyCollected must be a number." data-val="true">
                    @*@Html.EditorFor(Function(model) model.MoneyCollected, New With {.type = "number"})*@
                    @Html.ValidationMessageFor(Function(model) model.MoneyCollected)
                </div>
                @Html.HiddenFor(Function(model) model.JobID)
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 col-xs-12">
                <button class="btn btn-block btn-success">Order Delivered</button>
            </div>
        </div>
    </div>

      End Using
    </div>
</div>

<script src="/Content/FixForm.js"></script>
<script>
    $(function () {
        //on Delivery Only
        $("#MoneyCollected").val("");
    })
</script>
@Html.Partial("~/Views/Shared/_autoRefresh.vbhtml")