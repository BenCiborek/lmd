﻿@Modeltype List(Of LMD.Business)
@Code
    Layout = Nothing
End Code

<table class="table">
    <thead>
        <tr>
            <th>LM ID</th>
            <th>Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
        </tr>
    </thead>
    <tbody>
        @For Each business In Model
            @<tr>
        <td>@business.LMID</td>
        <td>@business.Name</td>
        <td>@business.Address1</td>
        <td>@business.City</td>
        <td>@business.State</td>
        <td><a href="@Url.Action("Edit", New With {.id = business.ID})">Edit</a></td>
        </tr>
        Next
    </tbody>
</table>
