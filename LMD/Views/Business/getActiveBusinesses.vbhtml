﻿@modeltype List(Of LMD.Business)
@Code
    Layout = Nothing
End Code


<select class="form-control" name="Business" id="Business">
    <option value="">Select One</option>
    <option value="0">All Businesses</option>
    @For Each item In Model
        @<option value="@item.ID">@item.Name</option>
    Next
</select>