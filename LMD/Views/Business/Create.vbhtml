﻿@ModelType lmd.Business
@Code
    ViewData("Title") = " | Business | Create"
End Code

<h2>Add Business</h2>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        @Using Html.BeginForm()
            @Html.EditorForModel()
            @<button class="btn btn-primary">
                Submit
            </button>
        End Using
    </div> 
</div>



<script src="/Content/FixForm.js"></script>

<script>
    $(function () {
        //on Create Only
        $("#ID").val("0");
    })
</script>
