﻿@Modeltype List(Of LMD.Job)
@code
    Layout = nothing
End Code

<table class="table">
    <thead>
        <tr>
            <th>Scheduled Date</th>
            <th>Address</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @For Each Jobs In Model.OrderByDescending(Function(o) o.ScheduledPickupDate).Take(50)
            @<tr>
                <td>@Jobs.ScheduledPickupDate.ToString("h:mm tt - M/d")<br />
                    @Jobs.Business.Name<br />
                    @Html.Action("JobStatus", New With {.id = Jobs.JobStatus})<br />
                    @Jobs.DriverID - @Jobs.Priority - @IIf(Jobs.ExpectedArrivalTime <> #1/1/1900#, Jobs.ExpectedArrivalTime.ToString("h:mm tt"),"")
                </td>
                <td>
                    @Jobs.DeliveryName<br />
                    @Jobs.DeliveryAddress, @Jobs.DeliveryAddress2<br />
                    @*@Jobs.DeliveryCity, @Jobs.DeliveryState @Jobs.DeliveryZip*@
                    @Jobs.DeliveryPhone <br />
                    @If Jobs.ActualPickupDate <> #1/1/1900# Then
                        @<span>@DateDiff(DateInterval.Minute, Jobs.ScheduledPickupDate, TimeZoneInfo.ConvertTimeFromUtc(Jobs.ActualPickupDate, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))) - </span>
                    End If
                    @If Jobs.DeliveryDate <> #1/1/1900# Then
                        @<span>
                            @DateDiff(DateInterval.Minute, Jobs.ActualPickupDate, Jobs.DeliveryDate) -
                            @DateDiff(DateInterval.Minute, Jobs.CreateDate, Jobs.DeliveryDate)
                        </span>
                    End If


                </td>
                 <td>
                     @If {1, 2, 3}.Contains(Jobs.JobStatus) Then
                        @<a href="@Url.Action("void",New With {.id = Jobs.ID})" class="btn btn-danger btn-sm">Void</a>
                     End If
                 </td>
                <td>
                    @If Not {1}.Contains(Jobs.JobStatus) Then
                        @<a href="@Url.Action("Reset", New With {.id = Jobs.ID})" class="btn btn-warning btn-sm">Reset</a>
                    End If
                </td>
            </tr>
                     Next
    </tbody>
</table>