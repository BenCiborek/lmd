﻿@ModelType LMD.Job
@Code
    ViewData("Title") = " | Job | Create"
    Dim times As List(Of DateTime) = ViewBag.Times
    Dim currentselectedtime As DateTime
    If Not Model Is Nothing Then
        currentselectedtime = Model.ScheduledPickupDate
    End If
End Code

<h2>New Job</h2>
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        @Using Html.BeginForm()
           @<div>
                <div>
                    @Html.ValidationSummary(True)
                </div>
                <div class="form-group editor-field">
                    <select id="ScheduledPickupDate" name="ScheduledPickupDate" class="form-control">
                        <option value="">Pickup Time</option>
                        @For Each time In times
                            @<option value="@time" @IIf(currentselectedtime = time, "Selected", "")>@time</option>
                        Next
                    </select>
                    @Html.ValidationMessageFor(Function(model) model.ScheduledPickupDate)
                    @*@Html.LabelFor(Function(model) model.ScheduledPickupDate)
            @Html.EditorFor(Function(model) model.ScheduledPickupDate)*@

                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryNumber)
                    @Html.EditorFor(Function(model) model.DeliveryNumber)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryNumber)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryType)
                    <select id="deliveryType" name="deliveryType" class="form-control">
                        <option value="">Select A Collection Method</option>
                        <option value="Paid and Tipped">Paid and Tipped</option>
                        <option value="Paid Needs Reciept Signed">Paid Needs Reciept Signed</option>
                        <option value="Collect Cash">Collect Cash</option>
                        <option value="Local Munchies Order">Local Munchies Order</option>
                    </select>
                    @Html.ValidationMessageFor(Function(model) model.DeliveryType)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.MoneyToCollect)
                    @Html.EditorFor(Function(model) model.MoneyToCollect)
                    @Html.ValidationMessageFor(Function(model) model.MoneyToCollect)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.TipCollected)
                    @Html.EditorFor(Function(model) model.TipCollected)
                    @Html.ValidationMessageFor(Function(model) model.TipCollected)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.NotesToDriver)
                    @Html.EditorFor(Function(model) model.NotesToDriver)
                    @Html.ValidationMessageFor(Function(model) model.NotesToDriver)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryName)
                    @Html.EditorFor(Function(model) model.DeliveryName)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryName)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryPhone)
                    @Html.EditorFor(Function(model) model.DeliveryPhone)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryPhone)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryAddress)
                    @Html.EditorFor(Function(model) model.DeliveryAddress)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryAddress)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryAddress2)
                    @Html.EditorFor(Function(model) model.DeliveryAddress2)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryAddress2)
                </div>
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryCity)
                    @Html.EditorFor(Function(model) model.DeliveryCity)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryCity)
                </div>
                <div class="form-group editor-field">
                        <label for="deliveryState">State</label>
                    <select id="deliveryState" name="deliveryState" class="form-control">
                        <option value="">Select State</option>
                        <option value='AL'>AL</option>
                        <option value='AK'>AK</option>
                        <option value='AS'>AS</option>
                        <option value='AZ'>AZ</option>
                        <option value='AR'>AR</option>
                        <option value='CA'>CA</option>
                        <option value='CO'>CO</option>
                        <option value='CT'>CT</option>
                        <option value='DE'>DE</option>
                        <option value='DC'>DC</option>
                        <option value='FM'>FM</option>
                        <option value='FL'>FL</option>
                        <option value='GA'>GA</option>
                        <option value='GU'>GU</option>
                        <option value='HI'>HI</option>
                        <option value='ID'>ID</option>
                        <option value='IL'>IL</option>
                        <option value='IN'>IN</option>
                        <option value='IA'>IA</option>
                        <option value='KS'>KS</option>
                        <option value='KY'>KY</option>
                        <option value='LA'>LA</option>
                        <option value='ME'>ME</option>
                        <option value='MH'>MH</option>
                        <option value='MD'>MD</option>
                        <option value='MA'>MA</option>
                        <option value='MI'>MI</option>
                        <option value='MN'>MN</option>
                        <option value='MS'>MS</option>
                        <option value='MO'>MO</option>
                        <option value='MT'>MT</option>
                        <option value='NE'>NE</option>
                        <option value='NV'>NV</option>
                        <option value='NH'>NH</option>
                        <option value='NJ'>NJ</option>
                        <option value='NM'>NM</option>
                        <option value='NY'>NY</option>
                        <option value='NC'>NC</option>
                        <option value='ND'>ND</option>
                        <option value='MP'>MP</option>
                        <option value='OH'>OH</option>
                        <option value='OK'>OK</option>
                        <option value='OR'>OR</option>
                        <option value='PW'>PW</option>
                        <option value='PA'>PA</option>
                        <option value='PR'>PR</option>
                        <option value='RI'>RI</option>
                        <option value='SC'>SC</option>
                        <option value='SD'>SD</option>
                        <option value='TN'>TN</option>
                        <option value='TX'>TX</option>
                        <option value='UT'>UT</option>
                        <option value='VT'>VT</option>
                        <option value='VI'>VI</option>
                        <option value='VA'>VA</option>
                        <option value='WA'>WA</option>
                        <option value='WV'>WV</option>
                        <option value='WI'>WI</option>
                        <option value='WY'>WY</option>
                    </select>
                    @Html.ValidationMessageFor(Function(model) model.DeliveryState)
                    </div>
                    @*@Html.LabelFor(Function(model) model.DeliveryState)
                    @Html.EditorFor(Function(model) model.DeliveryState)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryState)*@
                
                <div class="form-group editor-field">
                    @Html.LabelFor(Function(model) model.DeliveryZip)
                    @Html.EditorFor(Function(model) model.DeliveryZip)
                    @Html.ValidationMessageFor(Function(model) model.DeliveryZip)
                </div>
        </div>
            

            
            @<button class="btn btn-primary btn-block" style="padding-top:10px;">
                Submit
            </button>
        End Using
    </div>
</div>

<script src="/Content/FixForm.js"></script>


<script>
    $("#deliveryState option[value='@Model.DeliveryState']").attr("selected", "selected");
    $("#deliveryType option[value='@Model.DeliveryType']").attr("selected", "selected");


    $("#deliveryType").change(function () {
      var str = "";
      if($("#deliveryType option:selected").val() == 'Collect Cash')
      {
          $("#MoneyToCollect").val('')
      } else
      {
          $("#MoneyToCollect").val('0.00')
      }

    })
</script>

@Html.Partial("~/Views/Shared/_autoRefresh.vbhtml")