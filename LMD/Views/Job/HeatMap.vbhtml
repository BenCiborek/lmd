﻿@Modeltype list(of  lmd.GeoCode)
@Code
    ' ViewData("Title") = "HeatMap"
    Layout = Nothing
End Code

<h2>HeatMap</h2>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=visualization"></script>

<script>
    geocoder = new google.maps.Geocoder();
    // Adding 500 Data Points
    var map, pointarray, heatmap;

    var geoary = [
     @For Each address In Model
        @:new google.maps.LatLng(@address.Lat, @address.Lon),
    Next


    ];


    function initialize() {

        var mapOptions = {
            zoom: 13,
            center: new google.maps.LatLng(41.153667, -81.357886),
            mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById('map-canvas'),
            mapOptions);

        var pointArray = new google.maps.MVCArray(geoary);

        heatmap = new google.maps.visualization.HeatmapLayer({
            data: pointArray
        });


        heatmap.setMap(map);
        heatmap.set('radius', heatmap.get('radius') ? null : 20);
    }

    google.maps.event.addDomListener(window, 'load', initialize);


</script>



<div id="map-canvas" style="height:400px"></div>