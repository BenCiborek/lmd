﻿@Modeltype List(Of Integer)
@Code
    ViewData("Title") = " | Job"
End Code

<h2>Jobs</h2>

<a href="@Url.Action("Create")" class="btn btn-default">Create</a>

@Html.Action("Show", New With {.businessids = Model})