﻿@Modeltype LMD.Job
@Code
    ViewData("Title") = " | Void"
End Code

<h2>Void Order: </h2>

@Html.Action("Zoom", New With {.job = Model})

@Using Html.BeginForm()
    @<div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="form-group editor-field">
                <label for="Reason">Reason for Voiding</label>
                <input type="text" id="Reason" name="Reason" class="form-control" />
            </div>
        </div>
    </div>
    @<div>
         <div class="col-sm-3 col-sm-offset-3 col-xs-6">
             <a class="btn btn-default btn-block" href="/">
                 Don't Void
             </a>
         </div>
        <div class="col-sm-3 col-xs-6">
            <button class="btn btn-danger btn-block">
                Void
            </button>
        </div>
    </div>
End Using
