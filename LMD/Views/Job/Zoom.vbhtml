﻿@Modeltype LMD.Job
@Code
    Layout = Nothing
End Code

<div class="row">
    <div class="col-sm-6 col-sm-offset-3 col-xs-12">
        <div class="well well-sm" >
            <table cellpadding="5px">
                <tr>
                    <td>
                        Business:
                    </td>
                    <td>
                        @Model.Business.Name
                    </td>
                </tr>
                <tr>
                    <td>
                        Pick-Up Time:
                    </td>
                    <td>
                        @Model.ScheduledPickupDate
                    </td>
                </tr>
                <tr>
                    <td>
                        Collection Option:
                    </td>
                    <td>
                        @Model.DeliveryType
                    </td>
                </tr>
                <tr>
                    <td>
                        Notes:
                    </td>
                    <td>
                        @Model.NotesToDriver
                    </td>
                </tr>
                <tr>
                    <td>
                        Order Number:
                    </td>
                    <td>
                        @Model.DeliveryNumber
                    </td>
                </tr>
                <tr>
                    <td>
                        Address:
                    </td>
                    <td>
                        @Model.DeliveryName<br />
                        @Model.DeliveryAddress <br />
                        @Model.DeliveryAddress2 @IIf(Model.DeliveryAddress2 <> "", "<br />", "")
                        @Model.DeliveryCity, @Model.DeliveryState @Model.DeliveryZip
                        @Model.DeliveryPhone
                    </td>
                </tr>



            </table>
        </div>
    </div>
</div>