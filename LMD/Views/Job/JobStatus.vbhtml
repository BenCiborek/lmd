﻿@modeltype  LMD.Status
@Code
    Layout = Nothing
End Code

@If Not Model Is Nothing Then
    @<span class="label label-@Model.CssClass">@Model.Message.ToString</span>
End If

