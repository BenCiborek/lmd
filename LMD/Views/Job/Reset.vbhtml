﻿@Modeltype LMD.Job
@Code
    ViewData("Title") = " | Reset"
End Code

<h2>Reset Order: </h2>

@Html.Action("Zoom", New With {.job = Model})

@Using Html.BeginForm("ResetJob", "Job", New With {.jobid = Model.ID})
      @<div>
        <div class="col-sm-3 col-sm-offset-3 col-xs-6">
            <a class="btn btn-default btn-block" href="/">
                Don't Reset
            </a>
        </div>
        <div class="col-sm-3 col-xs-6">
            <button class="btn btn-warning btn-block">
                Reset
            </button>
        </div>
    </div>
End Using