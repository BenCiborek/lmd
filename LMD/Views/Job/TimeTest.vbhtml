﻿@modeltype  datetime
@Code
    ViewData("Title") = "TimeTest"
    Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
    Dim EasternTime As DateTime = New DateTime(EasternNow.Year, EasternNow.Month, EasternNow.Day, Model.Hour, Model.Minute, 0, 0)
    Dim EasternTimeUniversal As DateTime = System.TimeZoneInfo.ConvertTimeToUtc(EasternTime, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
End Code
<div class="well">
    Time: @Model <br />
    Now: @Now <br />
    Easern Time: @EasternNow <br />
    Time + EasternTime: @EasternTime<br />
    Now Universal: @Now.ToUniversalTime<br />
    Time + EasternTime: @EasternTimeUniversal<br />
    <br />
    <br />

</div>

