﻿@Modeltype List(Of LMD.Job)
@Code
    Layout = Nothing
End Code

<table class="table">
    <thead>
        <tr>
            <th>Scheduled Date</th>
            <th>Status</th>
            <th>Address</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @For Each Jobs In Model.OrderBy(Function(o) o.ScheduledPickupDate)
            @<tr>
                <td>
                    @Jobs.ScheduledPickupDate <br />
                    @If Jobs.ExpectedArrivalTime <> #1/1/1900# Then
                        @<span class="text-danger">ETA: @Jobs.ExpectedArrivalTime.ToString("h:mm tt") </span>
                    End If
                </td>
                <td>@Html.Action("JobStatus", New With {.id = Jobs.JobStatus}) </td>
                <td>
                    @Jobs.DeliveryName<br />
                    @Jobs.DeliveryAddress<br />
                </td>
                <td>
                    <a href="@Url.Action("void",New With {.id = Jobs.ID})" class="btn btn-danger btn-sm">Void</a>
                </td>
            </tr>
        Next
    </tbody>
</table>