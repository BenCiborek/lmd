﻿@modeltype List(Of LMD.Driver)
@Code
    Layout = Nothing
End Code


<select class="form-control" name="Driver" id="Driver">
    <option value="">Select One</option>
    <option value="0">Not a Driver</option>
    @For Each item In Model
        @<option value="@item.ID">@item.Name</option>
    Next
</select>