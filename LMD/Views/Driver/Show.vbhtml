﻿@Modeltype List(Of LMD.Driver)
@Code
    Layout = Nothing
End Code

<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Number</th>
            <th>City</th>
            <th>State</th>
        </tr>
    </thead>
    <tbody>
        @For Each Driver In Model
            @<tr>
                <td>@Driver.ID</td>
                <td>@Driver.Name</td>
                <td>@Driver.Phone</td>
                <td>@Driver.City</td>
                <td>@Driver.State</td>
                <td><a href="@Url.Action("Edit", New With {.id = Driver.ID})">Edit</a></td>
            </tr>
        Next
    </tbody>
</table>
