﻿@modeltype LMD.BusinessCloseOutReportVM
@Code
    ViewData("Title") = " | Business Console | Close-Out Report"
    Dim TotalBusiness As Double = 0
    Dim TotalLM As Double = 0
    Dim TotalTip As Double = 0
    Dim TotalFee As Double = 0
    Dim TotalTotal As Double = 0
    
    Dim EndDate As DateTime = DateAdd(DateInterval.Day, -30, Now().Date)
    Dim startDate As DateTime = DateAdd(DateInterval.Day, 1, Now().Date())
    Dim HourStart As Integer = 5
    Dim LMAmount As Double = 0.00
    Dim BusAmount As Double = 0.0
    
    Dim LMCollectionList As List(Of String) = {"Collect Cash", "Local Munchies - Link", "Local Munchies Order"}.ToList
End Code

<h2>Close-Out Report <small>@Model.StartDate.ToString("MM/dd/yyyy") - @Model.EndDate.ToString("MM/dd/yyyy")</small></h2>
<div class="row">
    <div class="col-xs-3">
        <a class="btn btn-default"  href="@Url.Action("Index")">< Back</a>
    </div>
    <form>
        <div class="col-xs-3">
           
                 @Html.Action("GetCloseoutReportDropDown", New With {.startdate = startDate,
                                                                     .enddate = EndDate,
                                                                     .DropDownName = "start",
                                                                    .Starthour = HourStart,
                                                                     .SelectedDate = Model.StartDate})
          
        </div>
        <div class="col-xs-3">
            @Html.Action("GetCloseoutReportDropDown", New With {.startdate = startDate,
                                                                     .enddate = EndDate,
                                                                     .DropDownName = "finish",
                                                                    .Starthour = HourStart,
                                                                     .SelectedDate = Model.EndDate})
        </div>
        <div class="col-xs-3">
            <button type="submit" class="btn btn-primary">Update Report</button> 
        </div>
    </form>
</div>

<table class="table">
    <thead>
        <tr>
            <th>Scheduled Date</th>
            <th>Delivery Number</th>
            <th>Status</th>
            <th>Address</th>
            <th>Type</th>
            <th>Business Collected</th>
            <th>LM Collected</th>
            <th>Tip</th>
            <th>Fee</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @For Each Job In Model.Jobs.OrderBy(Function(o) o.ScheduledPickupDate)
            @<tr>
                <td>@Job.ScheduledPickupDate</td>
                <td>@Job.DeliveryNumber</td>
                <td>@Html.Action("JobStatus", "Job", New With {.id = Job.JobStatus}) </td>
                <td>
                    @Job.DeliveryName<br />
                    @Job.DeliveryAddress<br />
                </td>
                <td>
                    @Job.DeliveryType
                </td>
                <td>
                    @If LMCollectionList.Contains(Job.DeliveryType) Then
                    'LM HAS money
                    BusAmount = 0.0
                    Else
                    'Business Has the money
                    BusAmount = Job.MoneyToCollect * 1.0
                    TotalBusiness += Job.MoneyToCollect
                    End If
                    @FormatCurrency(BusAmount)
                </td>
                <td>
                    @If LMCollectionList.Contains(Job.DeliveryType) Then
                    'LM HAS money
                    LMAmount = Job.MoneyToCollect * 1.0
                    TotalLM += Job.MoneyToCollect
                    Else
                    'Business Has the money
                    LMAmount = 0.0
                    End If
                    @FormatCurrency(LMAmount)
                </td>
                <td>
                    @FormatCurrency(Job.TipCollected)
                </td>
                <td>
                    @FormatCurrency(Job.RestaurntFee)
                </td>
                <td>
                    @FormatCurrency(Job.MoneyToCollect - Job.RestaurntFee)
                </td>
            </tr>
            TotalTip += Job.TipCollected
            TotalFee += Job.RestaurntFee
            TotalTotal += Job.MoneyToCollect - Job.RestaurntFee
        Next
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>@FormatCurrency(TotalBusiness)</th>
            <th>@FormatCurrency(TotalLM)</th>
            <th>@FormatCurrency(TotalTip)</th>
            <th>@FormatCurrency(TotalFee)</th>
            <th>@FormatCurrency(TotalTotal)</th>
        </tr>
    </tfoot>
</table>