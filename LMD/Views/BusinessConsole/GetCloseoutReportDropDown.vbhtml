﻿@Modeltype LMD.CloseoutDateDropDownVM
@code
    Dim startdate = Model.startDate
    Layout = Nothing
End Code
<select id="@Model.dropDownName" name="@Model.dropDownName" class="form-control">
    @While startdate > Model.endDate
        @<option value="@DateAdd(DateInterval.Hour, Model.startHour, startdate)" @IIf(DateAdd(DateInterval.Hour, Model.startHour, startdate) = Model.SelectedDate, "selected", "")>@DateAdd(DateInterval.Hour, Model.startHour, startdate).ToString("MM/dd/yyyy")</option>
        startdate = DateAdd(DateInterval.Day, -1, startdate)
    End While
</select>