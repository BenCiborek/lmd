﻿@modeltype lmd.Business
@Code
    ViewData("Title") = " | Business Console | " & Model.Name
End Code

<h2>Welcome @Model.Name</h2>
<br />
<div class="row">
    <div class="col-xs-12">
        <a href="@Url.Action("CloseOutReport", "BusinessConsole", New With {.start = DateAdd(DateInterval.Hour, 5, Now.Date()).ToString("MM/dd/yyyy"), .finish = DateAdd(DateInterval.Hour, 5, DateAdd(DateInterval.Day, 1, Now.Date())).ToString("MM/dd/yyyy")})" class="btn btn-default">Close-Out Report</a>
    </div>
</div>

@Html.Action("ShowAlert", "Home", New With {.Type = "Business"})


@*Create Job*@
<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        <a href="@Url.Action("Create", "Job")" class="btn btn-block btn-primary">Create Job!</a>
    </div>
</div>

@*Job List*@
@Html.Action("Show", "Job", New With {.businessids = Model.ID, .statuses = "1,2,3"})
