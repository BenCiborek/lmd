﻿@Modeltype LMD.NewUserVM
@Code
    ViewData("Title") = " | Create User"
End Code


<div class="row">

    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        <form class="form-signin center-block" role="form" method="post" style="max-width: 400px;">
            <h2 class="form-signin-heading">Create User</h2>
            @If Not Model.Message Is Nothing Then
                @<div class="alert alert-danger" id="signinalert"><span id="signinalertmessage">@Model.Message</span></div>
            End If
            <div class="form-group">
                <label for="SIEmail">Email address</label>
                <input type="email" class="form-control" id="Email" name="Email" placeholder="Email" value="@Model.Email">
                <p class="text-danger"> @Html.ValidationMessageFor(Function(model) model.Email)</p>
            </div>
            <div class="form-group">
                <label for="SIPassword">Password</label>
                <input type="text" class="form-control" id="Password" name="Password" placeholder="Password" value="@Model.Password">
                <p class="text-danger"> @Html.ValidationMessageFor(Function(model) model.Password)</p>
            </div>
            <div class="form-group">                
                <label for="Roles">Roles</label>
                @Html.Action("getActiveRoles")
                <p class="text-danger">@Html.ValidationMessageFor(Function(model) model.Roles)</p>
            </div>
            <div class="form-group">
                <label for="Business">Business</label>
                @Html.Action("getActiveBusinesses", "Business")
                <p class="text-danger">@Html.ValidationMessageFor(Function(model) model.Business)</p>
            </div>
            <div class="form-group">
                <label for="Roles">Driver</label>
                @Html.Action("getActiveDrivers", "Driver")
                <p class="text-danger">@Html.ValidationMessageFor(Function(model) model.Driver)</p>

            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Create</button>
        </form>

    </div>
</div>