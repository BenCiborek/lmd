﻿@Modeltype LMD.NewUserVM
@Code
    ViewData("Title") = " | Edit User"
End Code

<div class="row">

    <div class="col-sm-4 col-sm-offset-4 col-xs-12">
        <form class="form-signin center-block" role="form" method="post" style="max-width: 400px;">
            <h2 class="form-signin-heading">Create User</h2>
            @If Not Model.Message Is Nothing Then
                @<div class="alert alert-danger" id="signinalert"><span id="signinalertmessage">@Model.Message</span></div>
            End If
            <div class="form-group">
                <label for="SIEmail">Email address</label>
                <input type="email" class="form-control" id="Email" name="Email" placeholder="Email" value="@Model.Email">
                <p class="text-danger"> @Html.ValidationMessageFor(Function(model) model.Email)</p>
            </div>
            <div class="form-group">
                <label for="Roles">Roles</label>
                @Html.Action("getActiveRoles", "Account", New With {.selected = Model.Roles})
                <p class="text-danger">@Html.ValidationMessageFor(Function(model) model.Roles)</p>
            </div>
            <div class="form-group">
                <label for="Business">Business</label>
                @Html.Action("getActiveBusinesses", "Business")
                <p class="text-danger">@Html.ValidationMessageFor(Function(model) model.Business)</p>
            </div>
            <div class="form-group">
                <label for="Roles">Driver</label>
                @Html.Action("getActiveDrivers", "Driver")
                <p class="text-danger">@Html.ValidationMessageFor(Function(model) model.Driver)</p>

            </div>
            @Html.HiddenFor(Function(model) model.id)
            <button class="btn btn-lg btn-primary btn-block" type="submit">Update</button>
        </form>

    </div>
</div>