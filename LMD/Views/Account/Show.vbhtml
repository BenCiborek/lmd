﻿@Modeltype List(Of LMD.user)
@Code
    Layout = Nothing
End Code

<table class="table">
    <thead>
        <tr>
            <th>Email</th>
            <th>Role</th>
        </tr>
    </thead>
    <tbody>
        @For Each Users In Model
            @<tr>
                <td>@Users.Email</td>
                <td>@Users.Roles</td>
                  <td><a href="@Url.Action("EditUser", New With {.id = Users.UserID})">Edit</a></td>
            </tr>
        Next
    </tbody>
</table>