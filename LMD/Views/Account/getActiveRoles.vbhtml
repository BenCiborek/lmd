﻿@modeltype LMD.getActiveRolesVM
@Code
    Layout = Nothing
End Code


<select class="form-control" name="Roles" id="Roles">
    <option value="">Select One</option>
    @For Each item In Model.Roles
        @<option value="@item.Name" @IIf(item.Name = Model.Selected, "selected", "") >@item.Name</option>
    Next
</select>