﻿<!DOCTYPE html>
<html>
<head>
    <title>LM Delivery @ViewData("Title")</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="ROBOTS" content="INDEX, FOLLOW">
    <meta name="description" content="LM Delivery">
    <meta name="msvalidate.01" content="3A44CC5B8FAC22086E142202AEC73DFF" />


    <link href="/Content/css/smoothness/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
    <link href="/Content/css/bootstrap.css" rel="stylesheet" media="screen">
    <link href="/Content/Site.css" rel="stylesheet" media="screen" />

    <script src="/Content/js/jquery-1.9.1.min.js"></script>
    <script src="/Content/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="/Content/js/bootstrap.min.js"></script>

    @*  @System.Web.Optimization.Styles.Render("~/Content/css")
        @System.Web.Optimization.Scripts.Render("~/bundles/jquery")*@



    <link rel="shortcut icon" href="/favicon.ico">


    <!-- Google Analytics -->
    @If (Not HttpContext.Current.IsDebuggingEnabled) Then
        @<script>
   
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-60302659-1', 'auto');
    ga('send', 'pageview');

    </script>
        
       
@<script type="text/javascript" id="inspectletjs">
	window.__insp = window.__insp || [];
	__insp.push(['wid', 1406449980]);
	(function() {
		function __ldinsp(){var insp = document.createElement('script'); insp.type = 'text/javascript'; insp.async = true; insp.id = "inspsync"; insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js'; var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(insp, x); }
		if (window.attachEvent){
			window.attachEvent('onload', __ldinsp);
		}else{
			window.addEventListener('load', __ldinsp, false);
		}
	})();
</script>

        End If

    <!-- End Google Analytics -->
</head>

<body>
    @Html.Action("Index", "Header")

    <div class="container">
        @RenderBody()


        @Html.Action("Index", "Footer")
    </div>






</body>
</html>

