﻿@code
    Layout = Nothing
End Code
<nav class="navbar navbar-default" role="navigation">
    <div class="container">
        <div class="navbar-header nav navbar-nav" style="width:100%">
            <a class="navbar-brand" href="/">LM Delivery</a>
            <div class="nav navbar-nav navbar-right pull-right">
                @Html.Action("LogonLogoff")
            </div>
        </div>
    </div>
    <!-- /.container -->
    </nav>
