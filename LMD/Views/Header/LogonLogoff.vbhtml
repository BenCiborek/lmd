﻿@modeltype LMD.LogonLogoffVM
@Code
    Layout = Nothing
End Code
@If Model.IsLoggedIn Then
    @<a class="btn btn-default" href="@Url.Action("Logoff", "Account")">Logoff</a>
    @<script>
        try {
            __insp.push(['identify', "@Model.UserEmail"]);
        }
        catch (err) {
        }
    </script>
Else
     @<a class="btn btn-default" href="@Url.Action("Login", "Account")">Logon</a>
End If