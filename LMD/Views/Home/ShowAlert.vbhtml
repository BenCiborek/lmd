﻿@modeltype List(Of LMD.AlertMessage)
@Code
    Layout = Nothing
End Code

@For Each Alert In Model
    @<div style="margin:10px 0;" class="alert alert-@Alert.CssClass" role="alert">@Alert.Message</div>
Next
