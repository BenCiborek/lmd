﻿Imports System.ComponentModel.DataAnnotations
Public Class DriverTime
    Public Property ID As Integer
    <Required()>
    Public Property DriverID As Integer
    <Required()>
    Public Property StartTime As DateTime
    <Required()>
    Public Property EndTime As DateTime
    <Required()>
    Public Property Type As Integer
    <Required()>
    <DataType(DataType.EmailAddress)>
    Public Property Email As String
    <Required()>
    <StringLength(15, MinimumLength:=10, ErrorMessage:="Please Enter a Phone Number")> _
    Public Property Phone As String
    Public Property LastAlerted As DateTime
End Class
