﻿Imports System.Data.Entity
Public Class BusinessModel
    Dim db As LMDEntities = New LMDEntities
    Function GetDefaults(ByVal businessId As Integer) As BusinessDefaults
        Dim businessDefaults = db.BusinessDefaults.Where(Function(x) x.BusinessID = businessId).FirstOrDefault
        Return businessDefaults
    End Function
    Function GetBussinessByUserName(ByVal UserName As String) As Business
        Dim user = db.Users.Where(Function(x) x.UserName = UserName).FirstOrDefault
        If user Is Nothing Then
            user.BusinessID = 0
        End If
        Dim business = db.Businesses.Where(Function(x) x.ID = user.BusinessID).FirstOrDefault
        Return business
    End Function
    Function getBussinessByLMID(ByVal LMID As String) As Business
        Dim business = db.Businesses.Where(Function(x) x.LMID = LMID).FirstOrDefault
        Return business
    End Function
End Class
