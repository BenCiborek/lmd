﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Public Class Job
    Public Property ID As Integer

    Public Property CreateDate As DateTime

    Public Property JobStatus As Integer

    Public Property BusinessID As Integer
    Public Overridable Property Business As Business

    Public Property EnterUserName As String

    <DisplayName("Collection Options: ")> _
    <Required()> _
    Public Property DeliveryType As String


    <DisplayName("Scheduled Pick Up Time: ")> _
    <Required()> _
    Public Property ScheduledPickupDate As DateTime
    Public Property ActualPickupDate As DateTime

    Public Property DeliveryDate As DateTime

    Public Property ActualDeliveryDate As DateTime

    <DisplayName("Notes To Driver: ")> _
    Public Property NotesToDriver As String

    Public Property DeliveryNotes As String

    Public Property DriverID As Integer

    <DisplayName("Order Number: ")> _
    Public Property DeliveryNumber As String

    <DisplayName("Delivery Name: ")> _
   <Required()> _
   <StringLength(50, MinimumLength:=1, ErrorMessage:="Please Enter a Delivery Name")> _
    Public Property DeliveryName As String
    <DisplayName("Delivery Phone: ")> _
<Required()> _
<StringLength(15, MinimumLength:=10, ErrorMessage:="Please Enter a Delivery Phone")> _
    Public Property DeliveryPhone As String
    <DisplayName("Delivery Address: ")> _
<Required()> _
<StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Delivery Address")> _
    Public Property DeliveryAddress As String
    <DisplayName("Delivery Apt#/Dorm#: ")> _
    Public Property DeliveryAddress2 As String
    <DisplayName("Delivery City: ")> _
<Required()> _
<StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Delivery City")> _
    Public Property DeliveryCity As String
    <DisplayName("Delivery State: ")> _
<Required()> _
<StringLength(2, MinimumLength:=2, ErrorMessage:="Please Enter a Delivery State")> _
    Public Property DeliveryState As String
    <DisplayName("Delivery Zipcode: ")> _
<Required()> _
<StringLength(10, MinimumLength:=2, ErrorMessage:="Please Enter a Delivery Zip")> _
    Public Property DeliveryZip As String


    <DisplayName("Money To Be Collected: ")> _
    <Required()> _
    Public Property MoneyToCollect As Double

    <DisplayName("Money Collected: ")> _
    <Required()> _
    Public Property MoneyCollected As Double

    <DisplayName("Tip Collected: ")> _
    <Required()> _
    Public Property TipCollected As Double

    Public Property RestaurntFee As Double

    Public Property Priority As Integer

    Public Property VoidDate As DateTime
    Public Property VoidReason As String
    Public Property VoidUser As Integer

    Public Property QBStatus As Integer

    Public Property DeliveryAddressLatitude As Double
    Public Property DeliveryAddressLongitude As Double

    'Expected Delivery Time
    Public Property ExpectedArrivalTime As DateTime

End Class
