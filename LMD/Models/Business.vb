﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Public Class Business
    Public Property ID As Integer

    <DisplayName("Local Munchies ID: ")> _
    Public Property LMID As Integer

    <DisplayName("Company Name: ")> _
    <Required()> _
    <StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Company Name")> _
    Public Property Name As String

    <DisplayName("Company Address: ")> _
    <Required()> _
    <StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Company Address")> _
    Public Property Address1 As String

    <StringLength(50, MinimumLength:=0, ErrorMessage:="Please Enter a Company Address 2")> _
    Public Property Address2 As String

    <DisplayName("Company City: ")> _
    <Required()> _
    <StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Company City")> _
    Public Property City As String

    <DisplayName("Company State: ")> _
    <Required()> _
    <StringLength(2, MinimumLength:=2, ErrorMessage:="Please Enter a Company State")> _
    Public Property State As String

    <DisplayName("Company Zipcode: ")> _
    <Required()> _
    <StringLength(10, MinimumLength:=2, ErrorMessage:="Please Enter a Company Zip")> _
    Public Property Zip As String

    <DisplayName("Company Phone: ")> _
    <Required()> _
    <StringLength(15, MinimumLength:=10, ErrorMessage:="Please Enter a Company Phone")> _
    Public Property Phone As String

    <DisplayName("Company Inactive: ")> _
    <Required()> _
    Public Property Inactive As Boolean


End Class
