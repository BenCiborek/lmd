﻿Imports System.Data.Entity
Public Class DriverModel
    Shared db As LMDEntities = New LMDEntities
    Dim connstr = System.Configuration.ConfigurationManager.ConnectionStrings("LMDProd").ConnectionString
    Function getOpenJobs() As List(Of Job)
        Dim jobs = getAvailableOpenJobs()

        Return jobs
    End Function

    Shared Function getDriverByUserName(ByVal username As String)
        Dim user As User = db.Users.Where(Function(x) x.UserName = username).FirstOrDefault
        Return db.Drivers.Find(user.DriverID)
    End Function
    Function takeAllJobs(ByVal DriverID As Integer, ByVal BusinessID As Integer, Optional ByVal lat As Double = 0, Optional ByVal lon As Double = 0)
        Dim allJobsForBusiness = db.Jobs.Where(Function(x) x.JobStatus = 1 And x.BusinessID = BusinessID).ToList()
        Dim expectedArrivalTime As DateTime = #1/1/1900#
        Dim StartAddress As String = ""
        Dim endAddress As String = allJobsForBusiness.First.Business.Address1 & ", " & allJobsForBusiness.First.Business.City & ", " & allJobsForBusiness.First.Business.State & ", " & allJobsForBusiness.First.Business.Zip
        Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))

        'if we do not have their current location
        If lat = 0 Or lon = 0 Then
            'Get location from last job
            Dim lastAction = getLastLocation(DriverID)
            If Not lastAction Is Nothing Then
                If {2, 3}.Contains(lastAction.JobStatus) Then
                    'Pickup
                    StartAddress = lastAction.Business.Address1 & ", " & lastAction.Business.City & ", " & lastAction.Business.State & ", " & lastAction.Business.Zip
                Else
                    'Delivery
                    StartAddress = lastAction.DeliveryAddress & ", " & lastAction.DeliveryCity & ", " & lastAction.DeliveryState & ", " & lastAction.DeliveryZip
                End If
                expectedArrivalTime = getEstArrivalTime(StartAddress, endAddress, EasternNow)
            End If
        Else
            'We have their current location
            StartAddress = lat & "," & lon
            expectedArrivalTime = getEstArrivalTime(StartAddress, endAddress, EasternNow)
        End If
        
        For Each commonJob In allJobsForBusiness
            Dim ErrorMessage As ErrorMessage = takeJob(commonJob.ID, DriverID, expectedArrivalTime)
            If ErrorMessage.isError = False Then
                'Return RedirectToAction("Index", New With {.Message = ErrorMessage.Message})
            End If
        Next
        Return True
    End Function
    Function takeJob(ByVal JobId As Integer, ByVal DriverID As Integer, ByVal expectedArrivalTime As DateTime) As ErrorMessage
        Dim job = db.Jobs.Find(JobId)
        Dim DriverJobs = db.Jobs.Where(Function(x) {1, 2, 3}.Contains(x.JobStatus) And x.DriverID = DriverID).ToList
        Dim ErrorMessage As New ErrorMessage
        If job.JobStatus <> 1 Then
            ErrorMessage.isError = True
            ErrorMessage.Message = "This Job has already been taken"


        ElseIf DriverID = 0 Then
            ErrorMessage.isError = True
            ErrorMessage.Message = "This User is not a Driver"


            'ElseIf DriverJobs.Count > 0 Then
            '    ErrorMessage.isError = True
            '    ErrorMessage.Message = "This Driver already has a Job"


        Else
            job.JobStatus = 2
            job.DriverID = DriverID
            job.ExpectedArrivalTime = expectedArrivalTime
            db.Entry(job).State = EntityState.Modified
            db.SaveChanges()
            ErrorMessage.isError = False
            ErrorMessage.Message = "Complete"
        End If
        Return ErrorMessage
    End Function

    Shared Function getAvailableOpenJobs() As List(Of Job)
        Dim minBuffer = 20

        Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
        EasternNow = DateAdd(DateInterval.Minute, minBuffer, EasternNow)


        Dim jobs = db.Jobs.Where(Function(x) x.JobStatus = 1 And
                                               x.ScheduledPickupDate < EasternNow
                                               ).ToList()
        Return jobs
    End Function


    Function getNextJob() As Job

        Dim job = getAvailableOpenJobs().OrderBy(Function(z) z.ScheduledPickupDate).FirstOrDefault()

        Return job
    End Function
    Function getPickup(ByVal JobId As Integer, ByVal DriverID As Integer)
        Dim job = db.Jobs.Where(Function(x) x.JobStatus = 2 And x.ID = JobId And x.DriverID = DriverID).FirstOrDefault()
        Return job
    End Function
    Function getDelivery(ByVal JobId As Integer, ByVal DriverID As Integer)
        Dim job = db.Jobs.Where(Function(x) x.JobStatus = 3 And x.ID = JobId And x.DriverID = DriverID).OrderBy(Function(x) x.Priority).FirstOrDefault()
        Return job
    End Function
    Function hasOpenJob(ByVal DriverID As Integer) As Job
        Dim job = db.Jobs.Where(Function(x) {1, 2, 3}.Contains(x.JobStatus) And x.DriverID = DriverID
                                    ).OrderBy(Function(x) x.Priority
                                                  ).ThenBy(Function(x) x.JobStatus
                                                               ).FirstOrDefault()
        Return job
    End Function

    Function jobPickedUp(ByVal jobid As Integer, ByVal DriverID As Integer)
        Dim job = db.Jobs.Find(jobid)
        If job.JobStatus <> 2 Or job.DriverID <> DriverID Then
            Throw New Exception("Job Already Picked up")
            Return False
        Else
            job.JobStatus = 3
            job.ActualPickupDate = Now.ToUniversalTime
            db.Entry(job).State = EntityState.Modified
            db.SaveChanges()
            Return True
        End If
    End Function
    Function jobDelivered(ByVal jobid As Integer, ByVal DriverID As Integer, ByVal MoneyCollected As Double)
        Dim job = db.Jobs.Find(jobid)
        If job.JobStatus <> 3 Or job.DriverID <> DriverID Then
            Throw New Exception("Job Already Delivered")
            Return False
        Else
            job.JobStatus = 4
            job.MoneyCollected = MoneyCollected
            job.DeliveryDate = Now.ToUniversalTime
            job.ActualDeliveryDate = Now.ToUniversalTime
            db.Entry(job).State = EntityState.Modified
            db.SaveChanges()
            Return True
        End If
    End Function
    Function DriverCashoutReport(ByVal runDate As String, Optional ByVal Driverid As Integer = 0, Optional ByVal startdate As String = "") As List(Of DriverCashVM)
        Dim DriverCashList As New List(Of DriverCashVM)
        Dim ObjConn As System.Data.SqlClient.SqlConnection
        Dim ObjCmd As System.Data.SqlClient.SqlCommand
        Dim ObjDR As System.Data.SqlClient.SqlDataReader

        'Create DB connection object
        ObjConn = New System.Data.SqlClient.SqlConnection(connstr)

        If startdate = "" Then
            startdate = runDate
        End If

        Dim sql As String = "select * from [LMD_Money_Report] where deldate between '" & startdate & "' and '" & runDate & "' "
        If Driverid <> 0 Then
            sql += " and driverid = " & Driverid
        End If

        'Open DB connection
        ObjConn.Open()

        'Create command Object
        ObjCmd = New System.Data.SqlClient.SqlCommand(sql, ObjConn)
        ObjCmd.CommandTimeout = 900000

        'Create data reader object
        ObjDR = ObjCmd.ExecuteReader()

        'Get product prices
        If ObjDR.HasRows Then
            While ObjDR.Read()
                Dim DriverCash As New DriverCashVM
                DriverCash.DeliveryDate = ObjDR.Item("DelDate").ToString
                DriverCash.DriverID = ObjDR.Item("DriverID").ToString
                DriverCash.MoneyCollected = ObjDR.Item("MoneyCollected").ToString
                DriverCash.MoneyToCollect = ObjDR.Item("MoneyToCollect").ToString
                DriverCash.TipCollected = ObjDR.Item("TipCollected").ToString
                DriverCash.Jobs = ObjDR.Item("Jobs").ToString
                DriverCashList.Add(DriverCash)
            End While
        End If
        ObjDR.Close()
        ObjCmd.Dispose()
        ObjConn.Dispose()
        Return DriverCashList
    End Function
    Function hasAlert(ByVal driverid As Integer)
        Dim timenow As DateTime = Now().ToUniversalTime
        Dim alerts = db.DriverTimes.Where(Function(x) x.StartTime < timenow And
                                                 x.EndTime > timenow And
                                                 x.DriverID = driverid).ToList()
        If alerts.Count > 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Function AssignDeliveryPriority(ByVal DriverID As Integer)
        Dim Jobs = db.Jobs.Where(Function(x) x.DriverID = DriverID And x.JobStatus = 3).OrderBy(Function(x) x.ActualPickupDate).ToList()


        'Call Google API to Get Best WayPoints
        Dim bestway As BestWay = getBestWay(Jobs)

        Dim count As Integer = 0 'Counter
        For Each Job In Jobs
            'Reorder Priority on these Jobs
            Job.Priority = bestway.Order.Item(count) + 10
            db.Entry(Job).State = EntityState.Modified
            db.SaveChanges()

            count += 1
        Next
        Return True
    End Function
    Function getBestWay(ByVal jobs As List(Of Job)) As BestWay

        Return getBestWayBeta(jobs)
    End Function

    Function getBestWayBeta(ByVal jobs As List(Of Job))
        'This method is used to delvier the orders as fast as possible
        'It does not care about getting the driver back to Kent
        'Created on: 4/21/2015
        'Last update on: 4/21/2015

        Dim Bestways As New List(Of BestWay)
        Dim bestway As New BestWay
        'Don't even try google if there is only 1 delivery
        If jobs.Count = 1 Then

            bestway.Order.Add(0)
            bestway.seconds = 0
        Else
            'Use Google to find which route is the fastest
            Dim AddressStr As String = ""
            Dim EndAdd As String = ""

            Dim lastpickup = jobs.OrderByDescending(Function(x) x.ActualPickupDate).First()
            Dim startaddess = lastpickup.Business.Address1 & " " & lastpickup.Business.City & ", " & lastpickup.Business.State & " " & lastpickup.Business.Zip



            Dim count = 0
            For Each Job In jobs
                EndAdd = Job.DeliveryAddress & " " & Job.DeliveryCity & ", " & Job.DeliveryState & " " & Job.DeliveryZip
                AddressStr = ""
                For Each Job2 In jobs
                    If Job2.ID <> Job.ID Then
                        AddressStr &= Job2.DeliveryAddress & " " & Job2.DeliveryCity & ", " & Job2.DeliveryState & " " & Job2.DeliveryZip & "|"
                    End If
                Next
                Dim Bestway1 As BestWay = New BestWay
                Bestway1 = googleAPIBestWay(startaddess, EndAdd, AddressStr)
                Bestway1.Order.Insert(count, jobs.Count - 1)


                Bestways.Add(Bestway1)


                count += 1
            Next

            bestway = Bestways.OrderBy(Function(x) x.seconds).First

        End If
        Return bestway
    End Function

    Function getBestWayAlpha(ByVal jobs As List(Of Job))
        'Using this method we are optimized for what will put us back in the middle of Kent making all the deliveries as fast as possible.


        'Get Addresses
        Dim AddressStr As String = ""
        Dim StartAdd As String = ""
        Dim EndAdd As String = "Kent, OH"
        For Each Job In jobs
            AddressStr &= Job.DeliveryAddress & " " & Job.DeliveryCity & ", " & Job.DeliveryState & " " & Job.DeliveryZip & "|"
            StartAdd = Job.Business.Address1 & " " & Job.Business.City & ", " & Job.Business.State & " " & Job.Business.Zip
        Next

        Return googleAPIBestWay(StartAdd, EndAdd, AddressStr)
    End Function

    Function googleAPIBestWay(ByVal StartAdd As String, ByVal EndAdd As String, ByVal AddressStr As String) As BestWay
        Dim RequestStr As String = "http://maps.googleapis.com/maps/api/directions/xml?origin=" & StartAdd & "&destination=" & EndAdd & "&waypoints=optimize:true|" & AddressStr & "&sensor=false"
        Dim xmlresponse As String = GetWebResponse(RequestStr)
        'Dim orderlst As New List(Of Integer)

        Dim bestway As New BestWay

        'Read the XML
        ' Create a new, empty XML document
        Dim document As New System.Xml.XmlDocument()

        ' Load the contents into the XML document
        document.LoadXml(xmlresponse)

        Dim waypoints = document.GetElementsByTagName("waypoint_index")
        Dim orderlist As New List(Of Integer)
        For Each waypoint In waypoints
            orderlist.Add(waypoint.innertext)
        Next
        bestway.Order = orderlist

        Dim Durations = document.SelectNodes("/DirectionsResponse/route/leg/duration/value")
        bestway.seconds = 0
        For Each duration In Durations
            bestway.seconds += CInt(duration.InnerText)
        Next
        'bestway.seconds = CInt(document.SelectNodes("/DirectionsResponse/route/leg/duration/value")(0).InnerText)

        Return bestway
    End Function


    Shared Function GetWebResponse(ByVal url As String) As String
        Dim Val As String = ""
        Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create(url)



        ' Call the remote site, and parse the data in a response object
        Dim response As System.Net.HttpWebResponse = request.GetResponse()

        ' Check if the response is OK (status code 200)
        If response.StatusCode = System.Net.HttpStatusCode.OK Then

            ' Parse the contents from the response to a stream object
            Dim stream As System.IO.Stream = response.GetResponseStream()
            ' Create a reader for the stream object
            Dim reader As New System.IO.StreamReader(stream)
            ' Read from the stream object using the reader, put the contents in a string
            Val = reader.ReadToEnd()
        Else
            Throw New Exception("Could not retrieve document from the URL, response code: " & response.StatusCode)
        End If

        Return Val
    End Function

    Function getOtherPickups(ByVal originalJob As Job) As List(Of Job)
        Dim jobs = db.Jobs.Where(Function(x) x.BusinessID = originalJob.BusinessID And
                                     {1, 2}.Contains(x.JobStatus) And
                                     {0, originalJob.DriverID}.Contains(x.DriverID)).ToList
        Return jobs
    End Function

    Function releaseJobs(ByVal DriverID As Integer, ByVal JobStatusToRelease As Integer)
        Dim jobs = db.Jobs.Where(Function(x) x.DriverID = DriverID And x.JobStatus = JobStatusToRelease).ToList
        For Each Job In jobs
            ResetJob(Job.ID)
        Next
        Return True
    End Function
    Function ResetJob(ByVal Jobid As Integer)
        Dim Job = db.Jobs.Find(Jobid)
        Try
            Job.JobStatus = 1
            Job.DriverID = 0
            Job.Priority = 0
            Job.ExpectedArrivalTime = #1/1/1900#
            db.Entry(Job).State = EntityState.Modified
            db.SaveChanges()
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Shared Function getDriverForBusiness(ByVal businessID As Integer) As Integer
        Dim jobs = db.Jobs.Where(Function(x) x.BusinessID = businessID And
                                     {1, 2}.Contains(x.JobStatus))
        Dim DriverID As Integer = 0
        For Each Job In jobs
            DriverID = Job.DriverID
        Next
        Return DriverID
    End Function

    Shared Function isThereAPickupReady(ByVal DriverID As Integer) As Job
        Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))
        Dim DriversJobs = currentJobs(DriverID)
        Dim DriversBussinesses = DriversJobs.Select(Function(x) x.BusinessID).Distinct().ToList()
        'Add 5 min
        EasternNow = DateAdd(DateInterval.Minute, 5, EasternNow)
        Dim pendingPickup = db.Jobs.Where(Function(x) x.JobStatus = 1 And
                                               x.ScheduledPickupDate < EasternNow And
                                               Not DriversBussinesses.Contains(x.BusinessID)
                                               ).OrderBy(Function(x) x.ScheduledPickupDate()
                                                             ).FirstOrDefault()

        Return pendingPickup
    End Function

    Shared Function currentJobs(ByVal DriverID As Integer) As List(Of Job)
        'Return the current Jobs for the driver
        Dim jobs = db.Jobs.Where(Function(x) {1, 2, 3}.Contains(x.JobStatus) And
                                     x.DriverID = DriverID).ToList()
        Return jobs
    End Function

    Shared Function readyForAnotherPickup(ByVal jobs As List(Of Job)) As Boolean
        'less than 3 orders in their car 
        'less than 2 pickups
        Dim BusinessCount = jobs.Select(Function(x) x.BusinessID).Distinct().Count()
        Dim JobCount = jobs.Count()

        If BusinessCount < 2 And JobCount < 3 Then
            Return True
        Else
            Return False
        End If
    End Function

    Shared Function driversCount()


        'Get Drivers on 
        Dim drivers = getDriversOnNow()

        Dim drivercount = drivers.Count()
        If drivercount = 0 Then
            drivercount = 1
        End If
        Return drivercount
    End Function
    Shared Function getDriversOnNow() As List(Of DriverTime)
        Dim timenow As DateTime = Now().ToUniversalTime
        Return db.DriverTimes.Where(Function(x) x.StartTime < timenow And
                                                  x.EndTime > timenow).ToList()
    End Function

    Shared Function getEstArrivalTime(ByVal StartAddress As String, ByVal EndAddress As String, ByVal StartTime As DateTime)
        Dim RetTime As DateTime = #1/1/1900#
        Try


            Dim RequestStr As String = "http://maps.googleapis.com/maps/api/directions/xml?origin=" & StartAddress & "&destination=" & EndAddress
            Dim xmlresponse As String = GetWebResponse(RequestStr)
            Dim orderlst As New List(Of Integer)

            'Read the XML
            ' Create a new, empty XML document
            Dim document As New System.Xml.XmlDocument()

            ' Load the contents into the XML document
            document.LoadXml(xmlresponse)

            Dim secs = CInt(document.SelectNodes("/DirectionsResponse/route/leg/duration/value")(0).InnerText)

            RetTime = DateAdd(DateInterval.Second, secs, StartTime)

        Catch ex As Exception

        End Try

        Return RetTime
    End Function
    Function getLastLocation(ByVal DriverID As Integer) As Job
        Dim minback = 2
        Dim timefrom = DateAdd(DateInterval.Minute, minback * -1, Now.ToUniversalTime)
        Dim lastPickup = db.Jobs.Where(Function(x) x.DriverID = DriverID).OrderByDescending(Function(x) x.ActualPickupDate).FirstOrDefault
        Dim lastDelivery = db.Jobs.Where(Function(x) x.DriverID = DriverID).OrderByDescending(Function(x) x.DeliveryDate).FirstOrDefault

        'If they at least have a pick up and delivery
        If Not lastPickup Is Nothing And Not lastDelivery Is Nothing Then
            If lastPickup.ActualPickupDate > lastDelivery.DeliveryDate Then
                'See if last pickup is within timeframe
                If lastPickup.ActualPickupDate > timefrom Then
                    Return lastPickup
                End If
            Else
                'See if last Delivery is within timeframe
                If lastDelivery.DeliveryDate > timefrom Then
                    Return lastDelivery
                End If
            End If
        End If

        Return Nothing
    End Function
End Class
