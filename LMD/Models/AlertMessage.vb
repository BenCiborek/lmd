﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Public Class AlertMessage
    Public Property Id As Integer
    <Required()> _
    Public Property Type As String
    <Required()> _
    Public Property Message As String

    <Required()> _
    Public Property CssClass As String

    <Required()> _
    Public Property Startdate As DateTime
    <Required()> _
    Public Property EndDate As DateTime

    Public Property Active As Boolean

End Class
