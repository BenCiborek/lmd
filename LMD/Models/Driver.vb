﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel

Public Class Driver
    Public Property ID As Integer

    <DisplayName("Driver Name: ")> _
    <Required()> _
    <StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Driver Name")> _
    Public Property Name As String

    <DisplayName("Driver Address: ")> _
    <Required()> _
    <StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Driver Address")> _
    Public Property Address1 As String

    <StringLength(50, MinimumLength:=0, ErrorMessage:="Please Enter a Driver Address 2")> _
    Public Property Address2 As String

    <DisplayName("Driver City: ")> _
    <Required()> _
    <StringLength(50, MinimumLength:=3, ErrorMessage:="Please Enter a Driver City")> _
    Public Property City As String

    <DisplayName("Driver State: ")> _
    <Required()> _
    <StringLength(2, MinimumLength:=2, ErrorMessage:="Please Enter a Driver State")> _
    Public Property State As String

    <DisplayName("Driver Zipcode: ")> _
    <Required()> _
    <StringLength(10, MinimumLength:=2, ErrorMessage:="Please Enter a Driver Zip")> _
    Public Property Zip As String

    <DisplayName("Driver Phone: ")> _
    <Required()> _
    <StringLength(15, MinimumLength:=10, ErrorMessage:="Please Enter a Driver Phone")> _
    Public Property Phone As String

    <DisplayName("Driver Inactive: ")> _
    <Required()> _
    Public Property Inactive As Boolean


End Class
