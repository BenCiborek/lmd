﻿
Imports System.Data.Entity
Public Class LMDEntities
    Inherits DbContext

    Public Property Users As DbSet(Of User)

    Public Property Emails As DbSet(Of Email)

    Public Property Businesses As DbSet(Of Business)

    Public Property Drivers As DbSet(Of Driver)

    Public Property Jobs As DbSet(Of Job)

    Public Property UserTypes As DbSet(Of UserType)

    Public Property BusinessDefaults As DbSet(Of BusinessDefaults)

    Public Property DriverTimes As DbSet(Of DriverTime)

    Public Property AlertMessages As DbSet(Of AlertMessage)


    'QuickBooks Info
    Public Property QBTokenInfos As DbSet(Of QBTokenInfo)
    Public Property QBClasses As DbSet(Of QBClass)

End Class
