﻿Imports System.Data.Entity
Imports Geocoding.Google
Public Class JobModel
    Shared db As LMDEntities = New LMDEntities
    Function Void(ByVal jobid As Integer, ByVal username As String, ByVal Reason As String) As Boolean
        Dim job = db.Jobs.Find(jobid)
        Dim user = db.Users.Where(Function(x) x.UserName = username).FirstOrDefault

        Dim AllowedtoVoid As Boolean = False
        Dim NewStatus As Integer = 5

        'Admin Rules
        If user.Roles = "Admin" And {1, 2, 3}.Contains(job.JobStatus) Then
            AllowedtoVoid = True
            NewStatus = 7
        End If

        'Business Rules
        If user.Roles = "Business" And {1, 2}.Contains(job.JobStatus) And job.BusinessID = user.BusinessID Then
            AllowedtoVoid = True
            NewStatus = 5
        End If

        'Driver Rules
        If user.Roles = "Driver" And {2, 3}.Contains(job.JobStatus) And job.DriverID = user.DriverID Then
            AllowedtoVoid = True
            NewStatus = 6
        End If


        If AllowedtoVoid Then
            Try
                'Update Status
                job.JobStatus = NewStatus
                job.VoidDate = Now.ToUniversalTime()
                job.VoidReason = Reason
                job.VoidUser = user.UserID
                db.Entry(job).State = EntityState.Modified
                db.SaveChanges()

                Return True
            Catch ex As Exception

                Return False
            End Try

        Else
            Return False
        End If

    End Function

    Shared Function getStartTimeOffset(ByVal businessID As Integer) As Integer
        Dim retval = 15
        Try
            retval = getStartTimeBeta(businessID)
        Catch ex As Exception
            Dim email As New EmailModel
            email.AddEmail("E", "1", "bennyc66@gmail.com", "NoReply@LocalMunchiesDelivery.com", "Error Calculating New Start Time - Business Model", businessID & vbCrLf & vbCrLf & ex.ToString, False, "", 1)
        End Try
        Return retval
    End Function
    Shared Function getStartTimeAlpha(ByVal businessID As Integer)
        'No Longer Used
        'Stopped using on: 4/23/15

        'Assumptions
        Dim Offset As Integer = 15
        Dim BusinessesPerDriver = 2
        Dim MinuetsPerOverage = 10
        Dim openJobTimeDiff = 20
        Dim OverageMinuets = 0


        'bump up 10 min if there are more than 2 pending jobs per driver
        'unless the business already has one pending
        'then it is back down to 15

        Try


            Dim timenow As DateTime = Now().ToUniversalTime
            Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))


            Dim drivercount = DriverModel.driversCount()

            Dim timeTo As DateTime = DateAdd(DateInterval.Minute, openJobTimeDiff, EasternNow)
            'Get Open Jobs < timeto
            Dim Jobs = db.Jobs.Where(Function(x) {1}.Contains(x.JobStatus) And
                                          x.ScheduledPickupDate < timeTo).ToList

            'Get the businesses
            Dim Businesses = Jobs.Select(Function(x) x.BusinessID).Distinct().ToList()

            'If This Job is for a business that we already have then No new offset
            If Not Businesses.Contains(businessID) Then
                'If this is a new business get offset
                OverageMinuets = Math.Floor(Businesses.Count() / (BusinessesPerDriver * drivercount)) * MinuetsPerOverage
            End If

        Catch ex As Exception
            Dim email As New EmailModel
            email.AddEmail("E", "1", "bennyc66@gmail.com", "NoReply@LocalMunchiesDelivery.com", "Error Calculating New Start Time - Business Model", businessID & vbCrLf & vbCrLf & ex.ToString, False, "", 1)
        End Try

        Return Offset + OverageMinuets
    End Function
    Shared Function getStartTimeBeta(ByVal businessID As Integer)
        'Created on 4/23/2015 Implemented 4/23/15

        Dim timenow As DateTime = Now().ToUniversalTime
        Dim EasternNow As DateTime = System.TimeZoneInfo.ConvertTime(Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"))

        Dim minimumWait = 15

        Dim minsPerOutForDelivery = 10
        Dim minsPerAssigned = 10

        Dim minsPerPending = 10

        Dim AssignedTimeDiff = 15

        Dim returnWait = 0



        Dim timeToAssigned = DateAdd(DateInterval.Minute, AssignedTimeDiff, EasternNow)



        Dim DriverBusyTimes As New List(Of Integer)

        'Get the Drivers that are on NOW
        Dim Drivers = DriverModel.getDriversOnNow()

        For Each Driver In Drivers
            'Calculate the next time this driver will be available 
            Dim DriverJobs = DriverModel.currentJobs(Driver.DriverID)
            Dim TotalDriverWait As Integer = 0

            'Out For Delivery waits
            TotalDriverWait += DriverJobs.Where(Function(x) x.JobStatus = 3).Count() * minsPerOutForDelivery

            'Assigned Waits
            TotalDriverWait += DriverJobs.Where(Function(x) x.JobStatus = 2 And x.ScheduledPickupDate < timeToAssigned).Count() * minsPerAssigned


            DriverBusyTimes.Add(TotalDriverWait)
        Next

        Dim drivercount = DriverModel.driversCount()
        Dim PendingCount = db.Jobs.Where(Function(x) x.JobStatus = 1).Count()
        Dim PendingWait = (PendingCount / drivercount) * minsPerPending


        Dim DriverMinimumWait = 0
        If DriverBusyTimes.Count() > 0 Then
            DriverMinimumWait = DriverBusyTimes.Min()
        End If

        If PendingWait > DriverMinimumWait Then
            returnWait = PendingWait
        Else
            returnWait = DriverMinimumWait
        End If

        If returnWait < minimumWait Then
            returnWait = minimumWait
        End If
        Return returnWait
    End Function


    Shared Function AddressToGeocode(ByVal address As String)
        Dim Geocode As New GeoCode
        Geocode.Lat = 0
        Geocode.Lon = 0
        Try


            Dim geocoder As GoogleGeocoder = New GoogleGeocoder() With {.ApiKey = ConfigurationManager.AppSettings("GoogleAPIKey")}
            Dim addresses As GoogleAddress() = geocoder.Geocode(address)

            If address.Count > 0 Then
                Dim firstResult = addresses.First()
                Geocode.Lat = firstResult.Coordinates.Latitude
                Geocode.Lon = firstResult.Coordinates.Longitude
                Geocode.Address = firstResult.FormattedAddress
            End If
        Catch ex As Exception

        End Try
        Return Geocode
    End Function

End Class
