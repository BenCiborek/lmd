﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Public Class NewUserVM
    <Required()> _
    Public Property Business As Integer
    <Required()> _
    Public Property Roles As String

    <Required()> _
    <EmailAddress()> _
    Public Property Email As String

    <Required()> _
    Public Property Driver As Integer

    Public Property Message As String

    <Required()> _
    Public Property Password As String

    Public Property id As Integer


End Class
