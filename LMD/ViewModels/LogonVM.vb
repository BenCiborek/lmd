﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Public Class LogonVM
    <Required()> _
    Public Property Email As String
    <Required()> _
    Public Property Password As String
    Public Property ReturnURL As String
    Public Property Message As String
End Class
