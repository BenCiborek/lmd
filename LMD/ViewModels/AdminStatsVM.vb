﻿Public Class AdminStatsVM
    Public Property AvgTimeInCar As Integer
    Public Property AvgPickupTime As Integer
    Public Property MaxPickupTime As Integer
    Public Property MaxTimeInCar As Integer
    Public Property AvgTimeFromEntryToDelivery As Integer
    Public Property MaxTimeFromEntryToDelivery As Integer
End Class
