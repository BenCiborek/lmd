﻿Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel
Public Class DeliveryVM
    Public Property Job As Job
    Public Property JobID As Integer
    <Required()> _
    Public Property MoneyCollected As Double
End Class
