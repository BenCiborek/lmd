﻿Public Class QBSalesReceipt
    Public Property DocumentNumber As String
    Public Property DocumentDate As DateTime
    Public Property BAName As String
    Public Property BAAddr1 As String
    Public Property BAAddr2 As String
    Public Property BACity As String
    Public Property BAState As String
    Public Property BAZip As String
    Public Property PaymentMethodId As Integer
    Public Overridable Property Lines As List(Of QBLine)
End Class
